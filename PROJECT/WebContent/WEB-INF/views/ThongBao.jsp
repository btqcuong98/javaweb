<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <base href="${pageContext.servletContext.contextPath}/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <link href="https://fonts.googleapis.com/css?family=Big+Shoulders+Display|Old+Standard+TT&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="bootstrap/Login.css">
    <link rel="stylesheet" href="bootstrap/Home.css">


	<style>
	 #box{
	    background-color: white;
	    height: 330px;
	}
	
	.thongbao{
		text-align: center;
		size: 20px;
		color: blue;
		font-family: 'Big Shoulders Display', cursive;
    	font-family: 'Old Standard TT', serif;
	}
	
	.ghichu{
		size: 5px italicized;
		color: orange;
		font-family: 'Big Shoulders Display', cursive;
    	font-family: 'Old Standard TT', serif;
	}
	
	h2{
		color: red;
		size: bolder;
	}
	</style>

	
</head>
<body>

    <!--1 navigation -->
    <c:choose>
		<c:when test="${empty mlogin}">
		    <!-- navigation -->
		    <nav class="navbar navbar-default" role="navigation">
		        <div class="container">
		
		            <!-- header_logo -->
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                    <a class="navbar-brand mr-auto" href="#"><img src="bootstrap/img/logo.png" style="width:55px;"></a>
		            </div>
		            <!-- /header_logo -->
		
		            <div class="collapse navbar-collapse navbar-ex1-collapse">
		
		                <!-- Tìm kiếm -->
		                <form class="navbar-form navbar-left" role="search">
		                    <div class="form-group">
		                        <input type="text" class="form-control" placeholder="Lẩu, Nướng, Hải sản, ....">
		                    </div>
		                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Tìm kiếm</button>
		                </form>
		                <!-- /Timkiem -->
		
		                <!-- menu -->
		                <ul class="nav navbar-nav navbar-right">
		
		                    <!-- Trang chủ -->
		                    <li class="active">
		                        <a href="Home.htm">
		                            <span class="glyphicon glyphicon-home"></span> 
		                            Trang chủ
		                        </a>
		                    </li>
		
		                    <!-- Dropdown -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                            <span class="glyphicon glyphicon-map-marker"></span>
		                            Ăn uống 
		                            <b class="caret"></b>
		                        </a>
		                        <ul class="dropdown-menu">
		                            <li><a href="#">Lẩu</a></li>
		                            <li><a href="#">Nướng</a></li>
		                            <li><a href="#">Hải sản</a></li>
		                            <li><a href="#">Món chay</a></li>
		                            <li><a href="#">Đặt tiệc</a></li>
		                        </ul>
		                    </li>
		
		                    <!-- ưu đãi -->
		                    <li><a href="#"><span class="glyphicon glyphicon-fire"></span>Ưu đãi hot</a></li>
		
		                    <!-- Ngôn ngữ -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                                <span class="glyphicon glyphicon-globe"></span>Ngôn ngữ<b class="caret"></b></a>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="#">Tiếng Việt</a></span></li>
		                                <li class="res"><span><a href="#">English</a></span></li>
		                        </ul>
		                    </li>
		
		                    <!-- Tài khoản -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>Tài khoản</a>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="login.htm">Đăng nhập</a></span></li>
		                                <li class="res"><span><a href="register.htm">Đăng ký</a></span></li>
		                        </ul>
		                    </li> 
		                    <!-- /Tai Khoan -->
		
		                </ul>
		                <!-- /menu -->
		            </div><!-- /.navbar-collapse -->
		        </div> <!-- /container -->
		    </nav>
		    <!-- end navigation -->
		</c:when>
	    <c:otherwise>
		    <!-- navigation -->
		    <nav class="navbar navbar-default" role="navigation">
		        <div class="container">
		
		            <!-- header_logo -->
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                    <a class="navbar-brand mr-auto" href="#"><img src="bootstrap/img/logo.png" style="width:55px;"></a>
		            </div>
		            <!-- /header_logo -->
		
		            <div class="collapse navbar-collapse navbar-ex1-collapse">
		
		                <!-- Tìm kiếm -->
		                <form class="navbar-form navbar-left" role="search">
		                    <div class="form-group">
		                        <input type="text" class="form-control" placeholder="Lẩu, Nướng, Hải sản, ....">
		                    </div>
		                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Tìm kiếm</button>
		                </form>
		                <!-- /Timkiem -->
		
		                <!-- menu -->
		                <ul class="nav navbar-nav navbar-right">
		
		                    <!-- Trang chủ -->
		                    <li class="active">
		                        <a href="Home.htm">
		                            <span class="glyphicon glyphicon-home"></span> 
		                            Trang chủ
		                        </a>
		                    </li>
		
		                    <!-- Dropdown -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                            <span class="glyphicon glyphicon-map-marker"></span>
		                            Ăn uống 
		                            <b class="caret"></b>
		                        </a>
		                        <ul class="dropdown-menu">
		                            <li><a href="#">Lẩu</a></li>
		                            <li><a href="#">Nướng</a></li>
		                            <li><a href="#">Hải sản</a></li>
		                            <li><a href="#">Món chay</a></li>
		                            <li><a href="#">Đặt tiệc</a></li>
		                        </ul>
		                    </li>
		
		                    <!-- ưu đãi -->
		                    <li><a href="#"><span class="glyphicon glyphicon-fire"></span>Ưu đãi hot</a></li>
		
		                    <!-- Ngôn ngữ -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                                <span class="glyphicon glyphicon-globe"></span>Ngôn ngữ<b class="caret"></b></a>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="#">Tiếng Việt</a></span></li>
		                                <li class="res"><span><a href="#">English</a></span></li>
		                        </ul>
		                    </li>
		
		                    <!-- Tài khoản -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>${mlogin}</a>
		                        <c:choose>
		                        <c:when test="${admin == false}">
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="trangCaNhan.htm">Trang cá nhân</a></span></li>
		                                <li class="res"><span><a href="logout.htm">Đăng xuất</a></span></li>
		                        </ul>
		                        </c:when>
		                        <c:otherwise>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="trangCaNhan.htm">Trang cá nhân</a></span></li>
		                        		<li class="ad"><span><a href="tkNhaHang.htm">Quản lý</a></span></li>
		                                <li class="res"><span><a href="logout.htm">Đăng xuất</a></span></li>
		                        </ul>		
		                        </c:otherwise>
		                        </c:choose>
		                    </li> 
		                    <!-- /Tai Khoan -->
		                </ul>
		                <!-- /menu -->
		            </div><!-- /.navbar-collapse -->
		        </div> <!-- /container -->
		    </nav>
		    <!-- end navigation -->
	    </c:otherwise>
	</c:choose>
    <!-- end navigation -->


    <!-- tạo form login -->
   	<div class="container-fluid"> 
        <div class="row-fluid"> 
           <div class="col-md-offset-4 col-md-4" id="box"> 
               <h2>THÔNG BÁO</h2> 
               <hr> 
               <form class="form-horizontal" action="muahang/${MaNH}.htm" method="post" id="login_form"> 
	               <fieldset> 
	                   <div class="form-group"> 
	                       <div class="col-md-12"> 
	                           <br>
	                           <div class="input-group thongbao">
	                           		${thongbao}
	                           </div> 
	                       </div> 
	                   </div> 

	                   <div class="form-group"> 
	                       <div class="col-md-12"> 
	                           <div class="input-group ghichu">
	                           		${ghichu}
	                           </div> 
	                       </div> 
	                   </div> 
	                   <br>
	                   <div class="form-group"> 
	                       <div class="col-md-12"> 
	                           <button type="submit" class="btn btn-lg btn-primary btn-block">OK</button>
	                       </div> 
	                   </div> 
	               </fieldset> 
               </form> 
           </div> 
       </div>
   </div>

    
    <!-- /tạo form login -->

    <!-- Hướng dẫn -->
    <div class="HD">
        <div class="container">
                <img src="bootstrap/img/hd.png">
        </div>
    </div>
    <!-- /hướng dẫn -->

</body>
</html>