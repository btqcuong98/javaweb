<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <base href="${pageContext.servletContext.contextPath}/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <!-- gọi datepicker -->
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    
    <link href="https://fonts.googleapis.com/css?family=Big+Shoulders+Display|Old+Standard+TT&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="bootstrap/Register.css">
    <link rel="stylesheet" href="bootstrap/Home.css">

	<script type="text/javascript">
		$(function () {  
		$("#datepicker").datepicker({         
		autoclose: true,         
		todayHighlight: true 
		}).datepicker('update', new Date());
		});
	</script>

</head>
<body>

    <!-- navigation -->
    <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                 <!-- logo -->
                 <a class="navbar-brand mr-auto" href="#"><img src="bootstrap/img/logo.png" style="width:55px;"></a>
     
                 <!-- hiển thị trên thiết bị nhỏ hơn -->
                 <div class="navbar-header">
                     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                         <span class="sr-only">Toggle navigation</span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                     </button>
                 </div>
             
                 <!-- Collect the nav links, forms, and other content for toggling -->
                 <div class="collapse navbar-collapse navbar-ex1-collapse">
     
                     <!-- Tìm kiếm -->
                     <form class="navbar-form navbar-left" role="search">
                         <div class="form-group">
                             <input type="text" class="form-control" placeholder="Lẩu, Nướng, Hải sản, ....">
                         </div>
                         <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Tìm kiếm</button>
                     </form>
     
                     <!-- menu -->
                     <ul class="nav navbar-nav navbar-right">
     
                         <!-- Trang chủ -->
                         <li class="active"><a href="Home.htm"><span class="glyphicon glyphicon-home"></span> Trang chủ</a></li>
     
                         <!-- Dropdown -->
                         <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                 <span class="glyphicon glyphicon-map-marker"></span>Ăn uống <b class="caret"></b></a>
                             <ul class="dropdown-menu">
                                 <li><a href="#">Lẩu</a></li>
                                 <li><a href="#">Nướng</a></li>
                                 <li><a href="#">Hải sản</a></li>
                                 <li><a href="#">Món chay</a></li>
                                 <li><a href="#">Đặt tiệc</a></li>
                             </ul>
                         </li>
     
                         <!-- ưu đãi -->
                         <li><a href="#"><span class="glyphicon glyphicon-fire"></span>Ưu đãi hot</a></li>
     
                         <!-- Ngôn ngữ -->
                         <li class="dropdown">
                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                         <span class="glyphicon glyphicon-globe"></span>Ngôn ngữ<b class="caret"></b></a>
                                 <ul class="dropdown-menu">
                                         <li class="login"><span><a href="#">Tiếng Việt</a></span></li>
                                         <li class="res"><span><a href="#">English</a></span></li>
                                 </ul>
                         </li>
     
                         <!-- Tài khoản -->
                         <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>Tài khoản</a>
                             <ul class="dropdown-menu">
                                     <li class="login"><span><a href="login.htm">Đăng nhập</a></span></li>
                                     <li class="res"><span><a href="register.htm">Đăng ký</a></span></li>
                             </ul>
                         </li>
     
                         
                     </ul>
                 </div><!-- /.navbar-collapse -->
             </div>
    </nav> 
    <!-- navigation end -->

    <!-- Tạo form Đăng ký -->
    <div class="container-fluid"> 
        <div class="row-fluid"> 
            <div class="col-md-offset-4 col-md-4" id="box"> 
                <legend><a href="#"><i class="glyphicon glyphicon-globe"></i></a> Đăng ký thành viên!</legend> 
				
                <form action="register.htm" method="post" class="form" role="form"> 
                    <div class="row"> 
                        <div class="col-xs-6 col-md-6"> 
                            <input class="form-control" name="firstname" placeholder="Họ" required="" autofocus="" type="text">
                        </div> 
                        <div class="col-xs-6 col-md-6"> 
                            <input class="form-control" name="lastname" placeholder="Tên" required="" autofocus="" type="text">
                        </div> 
                    </div>

                    <br>

                    <input class="form-control" name="email" placeholder="Email" required="" autofocus="" type="email"> 
                    
                    <font color="red" size="1px italicized">${errors}</font>
					<br>
					
					<input class="form-control" name="sdt" placeholder="Số điện thoại" required="" autofocus="" type="text" required> 
                    <br>
                    
                    <input class="form-control" name="password" placeholder="Mật khẩu" required="" autofocus="" type="password" required> 

					<font color="red" size="1px italicized">${repass}</font>
					<br>
					
                    <input class="form-control" name="repassword" placeholder="Nhập lại mật khẩu" required="" autofocus="" type="password" required> 
                    <br>

                    <label for=""> Ngày sinh</label> 
                    <div id="datepicker" class="input-group date" data-date-format="dd-mm-yyyy"> 
                            <input class="form-control" readonly=""  required="" autofocus="" name="date" type="">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> 
                     </div>
                     <br>

                    <br>

                    <label class="radio-inline"><input name="sex" value="Nam" type="radio" checked="checked">Nam</label> 
                    <label class="radio-inline"><input name="sex" value="Nữ" type="radio">Nữ </label> 
                    <br> 
                    <br>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Đăng ký</button> 
                </form> 
            </div> 
        </div>
    </div>
    <!-- /Tạo form Đăng ký -->

    <!-- Hướng dẫn -->
    <div class="HD">
        <div class="container">
            <img src="img/hd.png">
        </div>
    </div>
    <!-- /hướng dẫn -->
</body>
</html>