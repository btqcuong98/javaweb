<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <base href="${pageContext.servletContext.contextPath}/">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Thống kê Khách hàng</title>

        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/waves.min.css" type="text/css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
        
        
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	    
        <!-- viết css -->
        <link href="css/menu-light.css" type="text/css" rel="stylesheet">
       	<link href="css/style.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="bootstrap/Home.css">

		<!-- script jquery -->
        <script src="js/metisMenu.min.js"></script>
        <script src="js/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="js/pace.min.js"></script>
        <script src="js/waves.min.js"></script>
        <script src="js/jquery-jvectormap-world-mill-en.js"></script>
        <script type="text/javascript" src="js/custom.js"></script> 
        
        <!--page scripts-->
        <script src="js/data-tables/jquery.dataTables.js"></script>
        <script src="js/data-tables/dataTables.tableTools.js"></script>
        <script src="js/data-tables/dataTables.bootstrap.js"></script>
        <script src="js/data-tables/dataTables.responsive.js"></script>
        <script src="js/data-tables/tables-data.js"></script>
        
        <!-- Google Analytics:  -->
        <script>
            (function (i, s, o, g, r, a, m)
            {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function ()
                {
                    (i[r].q = i[r].q || []).push(arguments);
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '../../../../../www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-3560057-28', 'auto');
            ga('send', 'pageview');
        </script>
    </head>
    <body>
        <!--NAVIGATION -->
	    <c:choose>
			<c:when test="${empty mlogin}"> 
			<!-- navigation 1-->
			<nav class="navbar navbar-default" role="navigation">
			        <div class="container">
			            <!-- header_logo -->
			            <div class="navbar-header">
			                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			                    <span class="sr-only">Toggle navigation</span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
			                </button>
			                    <a class="navbar-brand mr-auto" href="#"><img src="bootstrap/img/logo.png" style="width:55px;"></a>
			            </div>
			            <!-- /header_logo -->
			
			            <div class="collapse navbar-collapse navbar-ex1-collapse">
			
			                <!-- Tìm kiếm -->
			                <form class="navbar-form navbar-left" role="search">
			                    <div class="form-group">
			                        <input type="text" class="form-control" placeholder="Lẩu, Nướng, Hải sản, ....">
			                    </div>
			                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Tìm kiếm</button>
			                </form>
			                <!-- /Timkiem -->
			
			                <!-- menu -->
			                <ul class="nav navbar-nav navbar-right">
			
			                    <!-- Trang chủ -->
			                    <li class="active">
			                        <a href="Home.htm">
			                            <span class="glyphicon glyphicon-home"></span> 
			                            Trang chủ
			                        </a>
			                    </li>
			
			                    <!-- Dropdown -->
			                    <li class="dropdown">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			                            <span class="glyphicon glyphicon-map-marker"></span>
			                            Ăn uống 
			                            <b class="caret"></b>
			                        </a>
			                        <ul class="dropdown-menu">
			                            <li><a href="#">Lẩu</a></li>
			                            <li><a href="#">Nướng</a></li>
			                            <li><a href="#">Hải sản</a></li>
			                            <li><a href="#">Món chay</a></li>
			                            <li><a href="#">Đặt tiệc</a></li>
			                        </ul>
			                    </li>
			
			                    <!-- ưu đãi -->
			                    <li><a href="#"><span class="glyphicon glyphicon-fire"></span>Ưu đãi hot</a></li>
			
			                    <!-- Ngôn ngữ -->
			                    <li class="dropdown">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			                                                <span class="glyphicon glyphicon-globe"></span>Ngôn ngữ<b class="caret"></b></a>
			                        <ul class="dropdown-menu">
			                                <li class="login"><span><a href="#">Tiếng Việt</a></span></li>
			                                <li class="res"><span><a href="#">English</a></span></li>
			                        </ul>
			                    </li>
			
			                    <!-- Tài khoản -->
			                    <li class="dropdown">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>Tài khoản</a>
			                        <ul class="dropdown-menu">
			                                <li class="login"><span><a href="login.htm">Đăng nhập</a></span></li>
			                                <li class="res"><span><a href="register.htm">Đăng ký</a></span></li>
			                        </ul>
			                    </li> 
			                    <!-- /Tai Khoan -->
			
			                </ul>
			                <!-- /menu -->
			            </div><!-- /.navbar-collapse -->
			        </div> <!-- /container -->
			    </nav>
			    <!-- end navigation 1 -->
			</c:when>
			
		    <c:otherwise>  
		    	<!-- navigation 2-->
			    <nav class="navbar navbar-default" role="navigation">
			        <div class="container">
			
			            <!-- header_logo -->
			            <div class="navbar-header">
			                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			                    <span class="sr-only">Toggle navigation</span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
			                </button>
			                    <a class="navbar-brand mr-auto" href="#"><img src="bootstrap/img/logo.png" style="width:55px;"></a>
			            </div>
			            <!-- /header_logo -->
			
			            <div class="collapse navbar-collapse navbar-ex1-collapse">
			
			                <!-- Tìm kiếm -->
			                <form class="navbar-form navbar-left" role="search">
			                    <div class="form-group">
			                        <input type="text" class="form-control" placeholder="Lẩu, Nướng, Hải sản, ....">
			                    </div>
			                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Tìm kiếm</button>
			                </form>
			                <!-- /Timkiem -->
			
			                <!-- menu -->
			                <ul class="nav navbar-nav navbar-right">
			
			                    <!-- Trang chủ -->
			                    <li class="active">
			                        <a href="Home.htm">
			                            <span class="glyphicon glyphicon-home"></span> 
			                            Trang chủ
			                        </a>
			                    </li>
			
			                    <!-- Dropdown -->
			                    <li class="dropdown">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			                            <span class="glyphicon glyphicon-map-marker"></span>
			                            Ăn uống 
			                            <b class="caret"></b>
			                        </a>
			                        <ul class="dropdown-menu">
			                            <li><a href="#">Lẩu</a></li>
			                            <li><a href="#">Nướng</a></li>
			                            <li><a href="#">Hải sản</a></li>
			                            <li><a href="#">Món chay</a></li>
			                            <li><a href="#">Đặt tiệc</a></li>
			                        </ul>
			                    </li>
			
			                    <!-- ưu đãi -->
			                    <li><a href="#"><span class="glyphicon glyphicon-fire"></span>Ưu đãi hot</a></li>
			
			                    <!-- Ngôn ngữ -->
			                    <li class="dropdown">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			                                                <span class="glyphicon glyphicon-globe"></span>Ngôn ngữ<b class="caret"></b></a>
			                        <ul class="dropdown-menu">
			                                <li class="login"><span><a href="#">Tiếng Việt</a></span></li>
			                                <li class="res"><span><a href="#">English</a></span></li>
			                        </ul>
			                    </li>
			
			                    <!-- Tài khoản -->
			                    <li class="dropdown">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>${mlogin}</a>
			                        <c:choose>
			                        <c:when test="${admin == false}">
			                        <ul class="dropdown-menu">
			                                <li class="login"><span><a href="trangCaNhan.htm">Trang cá nhân</a></span></li>
			                                <li class="res"><span><a href="logout.htm">Đăng xuất</a></span></li>
			                        </ul>
			                        </c:when>
			                        <c:otherwise>
			                        <ul class="dropdown-menu">
			                                <li class="login"><span><a href="trangCaNhan.htm">Trang cá nhân</a></span></li>
			                        		<li class="ad"><span><a href="tkNhaHang.htm">Quản lý</a></span></li>
			                                <li class="res"><span><a href="logout.htm">Đăng xuất</a></span></li>
			                        </ul>		
			                        </c:otherwise>
			                        </c:choose>
			                    </li> 
			                    <!-- /Tai Khoan -->
			                </ul>
			                <!-- /menu -->
			            </div><!-- /.navbar-collapse -->
			        </div> <!-- /container -->
			    </nav>    
			    <!-- end navigation 2 -->
		    </c:otherwise>
		</c:choose>
	    <!-- END NAVIGATION -->

        <!-- Static navbar -->
        <section class="page">
            <nav class="navbar-aside navbar-static-side" role="navigation">
                <div class="sidebar-collapse nano">
                    <div class="nano-content">
                        <!-- câu quản lý -->
                        <ul class="nav metismenu" id="side-menu">
                            <!-- hình đại diện -->
                            <li class="nav-header">
                                <div class="dropdown side-profile text-left"> 
                                    <span style="display: block;">
                                        <img alt="image" class="img-circle" src="images/hinh_dai_dien.jpg" width="40">
                                    </span>
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                        <span class="clear" style="display: block;"> 
                                            <span class="block m-t-xs"> 
                                                <strong class="font-bold">${mlogin }<b class="caret"></b></strong>
                                            </span>
                                        </span> 
                                    </a>
                                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                        <li><a href="trangCaNhan.htm"><i class="fa fa-user"></i>Thông tin tài khoản</a></li>
                                    </ul>
                                </div>
                            </li>
                            <!-- end hình đại diện -->
                            <li>
                                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">ĐẶT BÀN </span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li><a href="tkPhieuDat.htm">Chi tiết </a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">MAILBOX </span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li><a href="sendMail/${khDN}.htm">Gửi email</a></li>
                                </ul>
                            </li>
                            
                            <li class="active">
                                <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">NHÀ HÀNG</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li><a href="tkNhaHang.htm">Chi tiết</a></li> 
                                </ul>
                            </li>
                        
                            <li>
                                <a href="#"><i class="fa fa-users"></i> <span class="nav-label">NGƯỜI DÙNG</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li><a href="tkKhachHang.htm">Danh sách khách hàng</a></li>
                                    <li><a href="tkAdmin.htm">Danh sách admin</a></li>
                                </ul>
                            </li>
                        </ul>
                        <!-- end cây quản lý -->
                    </div>
                </div>
            </nav>

            <!-- Khách hàng -->
           
            <div id="wrapper">
                <div class="content-wrapper container">
                    <!-- page title -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title">
                                <h1>THÔNG TIN KHÁCH HÀNG<small></small></h1>
                                <ol class="breadcrumb">
                                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                                    <li class="active">Users</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- end page title-->

                    
                    <!-- table -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive table-commerce">
                                <table id="basic-datatables" class="table table-striped table-hover">
                                    <thead>
	                                    <tr>
	                                        <th> <strong>MÃ</strong> </th>  
	                                        <th><strong>HỌ TÊN</strong></th>                                           
	                                        <th><strong>EMAIL</strong></th> 
	                                        <th><strong>PASSWORD</strong></th>                  
	                                        <th><strong>SEX</strong></th>                                                                  
	                                        <th><strong>SDT</strong></th>
	                                        <th><strong>NGÀY</strong></th>
	                                        <th><strong>CẤP QUYỀN</strong>
	                                    </tr>
                                	</thead>
                                    
                                    <tbody>
	                                    <c:forEach var="run" items="${listKH}">
	                                        <tr>
		                                         <td>${run.getMaKH() }</td>
		                                         <td>${run.getHo()} ${run.getTen() }</td>
		                                         <td>${run.getEmail() }</td>
		                                         <td>*********</td>
		                                         <td>${run.getSex() }</td>
		                                         <td>*********</td>
		                                         <td>${run.getNgaySinh() }</td>
		                                         <th><a href="quyen/${run.getMaKH() }.htm">ok</a></th>
	                                     	</tr>
	                                   </c:forEach> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- end products -->




        </section>

        
    </body>

<!-- Mirrored from themeforestdemo.com/templates/shapebootstrap/html/be-admin/products.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 18 May 2017 03:26:24 GMT -->
</html>