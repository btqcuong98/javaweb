<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <base href="${pageContext.servletContext.contextPath}/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- gọi datepicker -->
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <!-- font chữ -->
    <link href="https://fonts.googleapis.com/css?family=Big+Shoulders+Display|Old+Standard+TT&display=swap" rel="stylesheet">
    <!-- css -->
    <link rel="stylesheet" href="bootstrap/Home.css">
    <link rel="stylesheet" href="bootstrap/MuaHang.css">

    <!-- scrip -->
    <script type="text/javascript">
        $(function () {  
        $("#datepicker").datepicker({         
        autoclose: true,         
        todayHighlight: true 
        }).datepicker('update', new Date());
        });
    </script>

	<script type="text/javascript">
	showConfirmation = function(title, message, success, cancel) {
	    title = title ? title : 'Are you sure?';
	    var modal = $("#main_confirmation");
	    modal.find(".modal-title").html(title).end()
	        .find(".modal-body").html(message).end()
	        .modal({ backdrop: 'static', keyboard: false })
	        .on('hidden.bs.modal', function () {
	        modal.unbind();
	    });
	    if (success) {
	        modal.one('click', '.modal-footer .btn-primary', success);
	    }
	    if (cancel) {
	        modal.one('click', '.modal-header .close, .modal-footer .btn-default', cancel);
	    }
	};

	$(document).on("click", ".delete-event, .delete-all-event", function(event){
	    event.preventDefault();
	    var self = $(this);
	    var url = $(this).data('url');
	    var success = function(){
	    }
	    var cancel = function(){
	    };
	    if (self.data('confirmation')) {
	        var title = self.data('confirmation-title') ? self.data('confirmation-title') : undefined;
	        var message = self.data('confirmation');
	        showConfirmation(title, message, success, cancel);
	    } else {
	        success();
	        
	    }
	});
	</script>

	
</head>
<body>
    <!--1 navigation -->
    <c:choose>
		<c:when test="${empty mlogin}">
		    <!-- navigation -->
		    <nav class="navbar navbar-default" role="navigation">
		        <div class="container">
		
		            <!-- header_logo -->
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                    <a class="navbar-brand mr-auto" href="#"><img src="bootstrap/img/logo.png" style="width:55px;"></a>
		            </div>
		            <!-- /header_logo -->
		
		            <div class="collapse navbar-collapse navbar-ex1-collapse">
		
		                <!-- Tìm kiếm -->
		                <form class="navbar-form navbar-left" role="search">
		                    <div class="form-group">
		                        <input type="text" class="form-control" placeholder="Lẩu, Nướng, Hải sản, ....">
		                    </div>
		                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Tìm kiếm</button>
		                </form>
		                <!-- /Timkiem -->
		
		                <!-- menu -->
		                <ul class="nav navbar-nav navbar-right">
		
		                    <!-- Trang chủ -->
		                    <li class="active">
		                        <a href="Home.htm">
		                            <span class="glyphicon glyphicon-home"></span> 
		                            Trang chủ
		                        </a>
		                    </li>
		
		                    <!-- Dropdown -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                            <span class="glyphicon glyphicon-map-marker"></span>
		                            Ăn uống 
		                            <b class="caret"></b>
		                        </a>
		                        <ul class="dropdown-menu">
		                            <li><a href="#">Lẩu</a></li>
		                            <li><a href="#">Nướng</a></li>
		                            <li><a href="#">Hải sản</a></li>
		                            <li><a href="#">Món chay</a></li>
		                            <li><a href="#">Đặt tiệc</a></li>
		                        </ul>
		                    </li>
		
		                    <!-- ưu đãi -->
		                    <li><a href="#"><span class="glyphicon glyphicon-fire"></span>Ưu đãi hot</a></li>
		
		                    <!-- Ngôn ngữ -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                                <span class="glyphicon glyphicon-globe"></span>Ngôn ngữ<b class="caret"></b></a>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="#">Tiếng Việt</a></span></li>
		                                <li class="res"><span><a href="#">English</a></span></li>
		                        </ul>
		                    </li>
		
		                    <!-- Tài khoản -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>Tài khoản</a>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="login.htm">Đăng nhập</a></span></li>
		                                <li class="res"><span><a href="register.htm">Đăng ký</a></span></li>
		                        </ul>
		                    </li> 
		                    <!-- /Tai Khoan -->
		
		                </ul>
		                <!-- /menu -->
		            </div><!-- /.navbar-collapse -->
		        </div> <!-- /container -->
		    </nav>
		    <!-- end navigation -->
		</c:when>
	    <c:otherwise>
		    <!-- navigation -->
		    <nav class="navbar navbar-default" role="navigation">
		        <div class="container">
		
		            <!-- header_logo -->
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                    <a class="navbar-brand mr-auto" href="#"><img src="bootstrap/img/logo.png" style="width:55px;"></a>
		            </div>
		            <!-- /header_logo -->
		
		            <div class="collapse navbar-collapse navbar-ex1-collapse">
		
		                <!-- Tìm kiếm -->
		                <form class="navbar-form navbar-left" role="search">
		                    <div class="form-group">
		                        <input type="text" class="form-control" placeholder="Lẩu, Nướng, Hải sản, ....">
		                    </div>
		                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Tìm kiếm</button>
		                </form>
		                <!-- /Timkiem -->
		
		                <!-- menu -->
		                <ul class="nav navbar-nav navbar-right">
		
		                    <!-- Trang chủ -->
		                    <li class="active">
		                        <a href="Home.htm">
		                            <span class="glyphicon glyphicon-home"></span> 
		                            Trang chủ
		                        </a>
		                    </li>
		
		                    <!-- Dropdown -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                            <span class="glyphicon glyphicon-map-marker"></span>
		                            Ăn uống 
		                            <b class="caret"></b>
		                        </a>
		                        <ul class="dropdown-menu">
		                            <li><a href="#">Lẩu</a></li>
		                            <li><a href="#">Nướng</a></li>
		                            <li><a href="#">Hải sản</a></li>
		                            <li><a href="#">Món chay</a></li>
		                            <li><a href="#">Đặt tiệc</a></li>
		                        </ul>
		                    </li>
		
		                    <!-- ưu đãi -->
		                    <li><a href="#"><span class="glyphicon glyphicon-fire"></span>Ưu đãi hot</a></li>
		
		                    <!-- Ngôn ngữ -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                                <span class="glyphicon glyphicon-globe"></span>Ngôn ngữ<b class="caret"></b></a>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="#">Tiếng Việt</a></span></li>
		                                <li class="res"><span><a href="#">English</a></span></li>
		                        </ul>
		                    </li>
		
		                    <!-- Tài khoản -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>${mlogin}</a>
		                        <c:choose>
		                        <c:when test="${admin == false}">
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="trangCaNhan.htm">Trang cá nhân</a></span></li>
		                                <li class="res"><span><a href="logout.htm">Đăng xuất</a></span></li>
		                        </ul>
		                        </c:when>
		                        <c:otherwise>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="trangCaNhan.htm">Trang cá nhân</a></span></li>
		                        		<li class="ad"><span><a href="tkNhaHang.htm">Quản lý</a></span></li>
		                                <li class="res"><span><a href="logout.htm">Đăng xuất</a></span></li>
		                        </ul>		
		                        </c:otherwise>
		                        </c:choose>
		                    </li> 
		                    <!-- /Tai Khoan -->
		                </ul>
		                <!-- /menu -->
		            </div><!-- /.navbar-collapse -->
		        </div> <!-- /container -->
		    </nav>
		    <!-- end navigation -->
	    </c:otherwise>
	</c:choose>
    <!-- end navigation -->

    <!--2 noidung -->
    <div class="container">
        <div class="info">
            <!-- slider -->
            
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <%-- <c:forEach var="run" items="${listHA}"> --%>
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
            
                <!-- Wrapper for slides -->
                
                <div class="carousel-inner slider-img">
	                <div class="item active">
	                        <div class="desc"></div>
	                        <img src="bootstrap/img/img2/ig2_1.jpg" alt="Chicago" style="width:100%;">
	                </div>
	                
	                <c:forEach var="run" items="${listHA}" >
	                    <div class="item">
	                        <div class="desc"></div>
	                        <img src="${run.getUrl() }" alt="Los Angeles" style="width:100%;">
	                    </div>
	            	</c:forEach>
                </div>
            		
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <%-- </c:forEach> --%>
            <!-- end slider -->
			<c:forEach var="run" items="${list}">
            <p class="name"><span class="glyphicon glyphicon-star"></span>${run.getTenNH() }</p>
            <p class="addr"><span class="glyphicon glyphicon-home"></span>${run.getDiaChi() }</p>
            <p class="time">${run.getThoiGianPhucVu() }</p>
            <p class="price">Khoảng giá:<span class="priceTB"> ${run.getKhoangGia() }</span></p>
            
        </div>
    </div>
    </c:forEach>
    <!-- end noidung -->

    <!--3 Đặt bàn -->
    <div class="container-fluid"> 
            <div class="row-fluid"> 
                <div class="col-md-offset-4 col-md-4" id="box"> 
                    <legend><a href="#"><i class="glyphicon glyphicon-globe"></i></a> Đăng ký bàn!</legend> 
                    <legend>
                        <p class="truoc30phut">Nên đặt trước 30 phút</p>
                        <p class="giuCho">Đặt bàn được giữ chỗ</p>
                    </legend>

                    <form action="thanhcong.htm" method="post" class="form" role="form"> 

                        <div id="datepicker" class="input-group date" data-date-format="yyyy-mm-dd"> 
                            <input class="form-control" readonly="" type="text" name="date">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> 
                        </div>
                        <br>

                        <select id="txtGioDen" name="hour" class = "form-control" placeholder="Giờ" required="" autofocus="" readonly="" >
                            <option>1:00</option>   <option>1:30</option>   <option>2:00</option>   <option>2:30</option>
                            <option>3:00</option>   <option>3:30</option>   <option>4:00</option>   <option>4:30</option>
                            <option>5:00</option>   <option>5:30</option>   <option>6:00</option>   <option>6:30</option>
                            <option>7:00</option>   <option>7:30</option>   <option>8:00</option>   <option>8:30</option>
                            <option>9:90</option>   <option>10:00</option>  <option>10:30</option>  <option>11:30</option>
                            <option>12:30</option>  <option>13:00</option>  <option>13:30</option>  <option>14:00</option>
                            <option>14:30</option>  <option>15:00</option>  <option>15:30</option>  <option>16:00</option>
                            <option>16:30</option>  <option>17:00</option>  <option>17:30</option>  <option>18:00</option>
                            <option>18:30</option>  <option>19:00</option>  <option>19:30</option>  <option>20:00</option>
                            <option>20:30</option>  <option>21:00</option>  <option>21:30</option>  <option>22:00</option>
                            <option>22:30</option>  <option>23:00</option>  <option>23:30</option>  <option>00:00</option>
                        </select>
                        <br>

                        <div class="row"> 
                            <div class="col-xs-6 col-md-6"> 
                                <select id="" name="nguoiLon" class="form-control" placeholder="Số lượng người lớn" required="" autofocus="" readonly="" >
                                    <option>1 người lớn </option>   <option>2 người lớn </option>   <option>3 người lớn </option> <option>4 người lớn </option> <option>5 người lớn </option>
                                    <option>6 người lớn </option>   <option>7 người lớn </option>   <option>8 người lớn </option> <option>9 người lớn </option> <option>10 người lớn </option>
                                    <option>11 người lớn </option>   <option>12 người lớn </option>   <option>13 người lớn </option> <option>14 người lớn </option> <option>15 người lớn </option>
                                    <option>16 người lớn </option>   <option>17 người lớn </option>   <option>18 người lớn </option> <option>19 người lớn </option> <option>20 người lớn </option>
                                    <option>21 người lớn </option>   <option>22 người lớn </option>   <option>23 người lớn </option> <option>24 người lớn </option> <option>25 người lớn </option>
                                    <option>26 người lớn </option>   <option>27 người lớn </option>   <option>28 người lớn </option> <option>29 người lớn </option> <option>30 người lớn </option>
                                    <option>31 người lớn </option>   <option>32 người lớn </option>   <option>33 người lớn </option> <option>34 người lớn </option> <option>35 người lớn </option>
                                    <option>36 người lớn </option>   <option>37 người lớn </option>   <option>38 người lớn </option> <option>39 người lớn </option> <option>40 người lớn </option>
                                    <option>41 người lớn </option>   <option>42 người lớn </option>   <option>43 người lớn </option> <option>44 người lớn </option> <option>45 người lớn </option>
                                    <option>56 người lớn </option>   <option>47 người lớn </option>   <option>48 người lớn </option> <option>49 người lớn </option> <option>50 người lớn </option>
                                    <option>61 người lớn </option>   <option>52 người lớn </option>   <option>53 người lớn </option> <option>54 người lớn </option> <option>55 người lớn </option>
                                    <option>66 người lớn </option>   <option>57 người lớn </option>   <option>58 người lớn </option> <option>59 người lớn </option> <option>60 người lớn </option>
                                    <option>71 người lớn </option>   <option>62 người lớn </option>   <option>63 người lớn </option> <option>64 người lớn </option> <option>65 người lớn </option>
                                    <option>76 người lớn </option>   <option>67 người lớn </option>   <option>68 người lớn </option> <option>69 người lớn </option> <option>70 người lớn </option>
                                    <option>81 người lớn </option>   <option>72 người lớn </option>   <option>73 người lớn </option> <option>74 người lớn </option> <option>75 người lớn </option>
                                    <option>86 người lớn </option>   <option>77 người lớn </option>   <option>78 người lớn </option> <option>79 người lớn </option> <option>80 người lớn </option>
                                    <option>91 người lớn </option>   <option>82 người lớn </option>   <option>83 người lớn </option> <option>84 người lớn </option> <option>85 người lớn </option>
                                    <option>96 người lớn </option>   <option>87 người lớn </option>   <option>88 người lớn </option> <option>89 người lớn </option> <option>90 người lớn </option>
                                    <option>91 người lớn </option>   <option>92 người lớn </option>   <option>93 người lớn </option> <option>94 người lớn </option> <option>95 người lớn </option>
                                    <option>96 người lớn </option>   <option>97 người lớn </option>   <option>98 người lớn </option> <option>99 người lớn </option> <option>100 người lớn </option>
                                </select>    
                            </div> 
                            <div class="col-xs-6 col-md-6"> 
                                    <select id="" name="treEm" class="form-control" placeholder="Số lượng trẻ em" required="" autofocus="" readonly="" >
                                            <option>1 trẻ em </option>   <option>2 trẻ em </option>   <option>3 trẻ em </option> <option>4 trẻ em </option> <option>5 trẻ em </option>
                                            <option>6 trẻ em </option>   <option>7 trẻ em </option>   <option>8 trẻ em </option> <option>9 trẻ em </option> <option>10 trẻ em </option>
                                            <option>11 trẻ em </option>   <option>12 trẻ em </option>   <option>13 trẻ em </option> <option>14 trẻ em </option> <option>15 trẻ em </option>
                                            <option>16 trẻ em </option>   <option>17 trẻ em </option>   <option>18 trẻ em </option> <option>19 trẻ em</option> <option>20 trẻ em </option>
                                            <option>21 trẻ em </option>   <option>22 trẻ em </option>   <option>23 trẻ em </option> <option>24 trẻ em </option> <option>25 trẻ em </option>
                                            <option>26 trẻ em </option>   <option>27 trẻ em </option>   <option>28 trẻ em </option> <option>29 trẻ em </option> <option>30 trẻ em </option>
                                            <option>31 trẻ em </option>   <option>32 trẻ em </option>   <option>33 trẻ em </option> <option>34 trẻ em </option> <option>35 trẻ em </option>
                                            <option>36 trẻ em </option>   <option>37 trẻ em </option>   <option>38 trẻ em </option> <option>39 trẻ em </option> <option>40 trẻ em </option>
                                            <option>41 trẻ em </option>   <option>42 trẻ em </option>   <option>43 trẻ em </option> <option>44 trẻ em </option> <option>45 trẻ em</option>
                                            <option>56 trẻ em </option>   <option>47 trẻ em </option>   <option>48 trẻ em </option> <option>49 trẻ em </option> <option>50 trẻ em </option>
                                            <option>61 trẻ em </option>   <option>52 trẻ em </option>   <option>53 trẻ em </option> <option>54 trẻ em </option> <option>55 trẻ em</option>
                                            <option>66 trẻ em </option>   <option>57 trẻ em </option>   <option>58 trẻ em </option> <option>59 trẻ em </option> <option>60 trẻ emn </option>
                                            <option>71 trẻ em </option>   <option>62 trẻ em </option>   <option>63 trẻ em </option> <option>64 trẻ em </option> <option>65 trẻ em </option>
                                            <option>76 trẻ em </option>   <option>67 trẻ em </option>   <option>68 trẻ em </option> <option>69 trẻ em </option> <option>70 trẻ em </option>
                                            <option>81 trẻ em </option>   <option>72 trẻ em </option>   <option>73 trẻ em </option> <option>74 trẻ em </option> <option>75 trẻ em </option>
                                            <option>86 trẻ em </option>   <option>77 trẻ em </option>   <option>78 trẻ em</option> <option>79 trẻ em </option> <option>80 trẻ em </option>
                                            <option>91 trẻ em </option>   <option>82 trẻ em </option>   <option>83 trẻ em </option> <option>84 trẻ em </option> <option>85 trẻ em </option>
                                            <option>96 trẻ em </option>   <option>87 trẻ em </option>   <option>88 trẻ em </option> <option>89 trẻ em </option> <option>90 trẻ em </option>
                                            <option>91 trẻ em </option>   <option>92 trẻ em </option>   <option>93 trẻ em </option> <option>94 trẻ em </option> <option>95 trẻ em </option>
                                            <option>96 trẻ em </option>   <option>97 trẻ em </option>   <option>98 trẻ em </option> <option>99 trẻ em </option> <option>100 trẻ em </option>
                                        </select>  
                            </div> 
                        </div>
                        <br>
                        
                        <textarea class="form-control" cols="70" name="note" placeholder="Viết thêm ghi chú ..."></textarea>
                        <!-- data-toggle="modal" data-target="#myModal" -->
                        <br>
                        
	                     <c:choose>
	                     	<c:when test="${empty mlogin}">
	                     		<button class="btn btn-lg btn-primary btn-block btn btn-danger delete-event" type="button" data-dismiss="modal" data-url="" data-confirmation="Bạn cần đăng nhập để đặt bàn" data-confirmation-title="THÔNG BÁO">Đăng ký</button>
	                     	</c:when>
	                     	<c:otherwise>
	                     		<!-- <button  type="submit" data-toggle="modal" class="btn btn-lg btn-primary btn-block" data-target="#ModalMy"> Đăng ký</button>  -->
	                     		<button  type="submit" class="btn btn-lg btn-primary btn-block"> Đăng ký</button> 
	                     	</c:otherwise>
	                     </c:choose>
	                     
                     </form> 
                        
                </div> 
            </div>
    </div>
    <!-- end đặt bàn -->
    
    <!-- thông báo -->
    <div id="main_confirmation" class="modal fade">
        <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header xacNhan">
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                       <h4 class="modal-title"></h4>
                  </div>
                  <div class="modal-body bd"></div>
                  <div class="modal-footer">
                        <button type="button" class="btn btn-default form-control btnHuy" data-dismiss="modal">OK</button>
                   </div>
              </div>
          </div>
     </div>
    <!-- end thong báo -->
    
    <!--4 slider -->
    <div id="slider" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#slider" data-slide-to="0" class="active"></li>
            <li data-target="#slider" data-slide-to="1"></li>
            <li data-target="#slider" data-slide-to="2"></li>
        </ol>
    
        <!-- Wrapper for slides -->
        <div class="carousel-inner slider-inner">
            <div class="item active">
                <div class="desc"></div>
                <img src="bootstrap/img/img1/mn1_4.jpg" alt="Los Angeles" style="width:100%;">
            </div>
            
    		<c:forEach var="r" items="${listMN}">
	            <div class="item">
	            	
	                <div class="desc"></div>
	                
	                <img src="${r.getUrl() }" alt="Chicago" style="width:100%;">
	            </div>
        	 </c:forEach> 
        </div>
    
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#slider" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#slider" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- end slider -->


    <!-- Hướng dẫn -->
   <div class="HD">
        <div class="container">
                <img src="bootstrap/img/hd.png">
        </div>
   </div>
    <!-- /hướng dẫn -->
    
</body>
</html>

