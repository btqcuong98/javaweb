<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <base href="${pageContext.servletContext.contextPath}/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update Phiếu đặt</title>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<!-- gọi datepicker -->
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>


    <link rel="stylesheet" href="bootstrap/addInsert.css">
    <link rel="stylesheet" href="bootstrap/Home.css">
    <link rel="stylesheet" href="bootstrap/Register.css">
    
    
    <script type="text/javascript">
		$(function () {  
		$("#datepicker").datepicker({         
		autoclose: true,         
		todayHighlight: true 
		}).datepicker('update', ngayD);
		});
	</script>
		
</head>
<body>
	<!--NAVIGATION -->
    <c:choose>
		<c:when test="${empty mlogin}"> 
		<!-- navigation 1-->
		<nav class="navbar navbar-default" role="navigation">
		        <div class="container">
		            <!-- header_logo -->
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                    <a class="navbar-brand mr-auto" href="#"><img src="bootstrap/img/logo.png" style="width:55px;"></a>
		            </div>
		            <!-- /header_logo -->
		
		            <div class="collapse navbar-collapse navbar-ex1-collapse">
		
		                <!-- Tìm kiếm -->
		                <form class="navbar-form navbar-left" role="search">
		                    <div class="form-group">
		                        <input type="text" class="form-control" placeholder="Lẩu, Nướng, Hải sản, ....">
		                    </div>
		                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Tìm kiếm</button>
		                </form>
		                <!-- /Timkiem -->
		
		                <!-- menu -->
		                <ul class="nav navbar-nav navbar-right">
		
		                    <!-- Trang chủ -->
		                    <li class="active">
		                        <a href="Home.htm">
		                            <span class="glyphicon glyphicon-home"></span> 
		                            Trang chủ
		                        </a>
		                    </li>
		
		                    <!-- Dropdown -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                            <span class="glyphicon glyphicon-map-marker"></span>
		                            Ăn uống 
		                            <b class="caret"></b>
		                        </a>
		                        <ul class="dropdown-menu">
		                            <li><a href="#">Lẩu</a></li>
		                            <li><a href="#">Nướng</a></li>
		                            <li><a href="#">Hải sản</a></li>
		                            <li><a href="#">Món chay</a></li>
		                            <li><a href="#">Đặt tiệc</a></li>
		                        </ul>
		                    </li>
		
		                    <!-- ưu đãi -->
		                    <li><a href="#"><span class="glyphicon glyphicon-fire"></span>Ưu đãi hot</a></li>
		
		                    <!-- Ngôn ngữ -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                                <span class="glyphicon glyphicon-globe"></span>Ngôn ngữ<b class="caret"></b></a>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="#">Tiếng Việt</a></span></li>
		                                <li class="res"><span><a href="#">English</a></span></li>
		                        </ul>
		                    </li>
		
		                    <!-- Tài khoản -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>Tài khoản</a>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="login.htm">Đăng nhập</a></span></li>
		                                <li class="res"><span><a href="register.htm">Đăng ký</a></span></li>
		                        </ul>
		                    </li> 
		                    <!-- /Tai Khoan -->
		
		                </ul>
		                <!-- /menu -->
		            </div><!-- /.navbar-collapse -->
		        </div> <!-- /container -->
		    </nav>
		    <!-- end navigation 1 -->
		</c:when>
		
		
		
	    <c:otherwise>  
	    	<!-- navigation 2-->
		    <nav class="navbar navbar-default" role="navigation">
		        <div class="container">
		
		            <!-- header_logo -->
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                    <a class="navbar-brand mr-auto" href="#"><img src="bootstrap/img/logo.png" style="width:55px;"></a>
		            </div>
		            <!-- /header_logo -->
		
		            <div class="collapse navbar-collapse navbar-ex1-collapse">
		
		                <!-- Tìm kiếm -->
		                <form class="navbar-form navbar-left" role="search">
		                    <div class="form-group">
		                        <input type="text" class="form-control" placeholder="Lẩu, Nướng, Hải sản, ....">
		                    </div>
		                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Tìm kiếm</button>
		                </form>
		                <!-- /Timkiem -->
		
		                <!-- menu -->
		                <ul class="nav navbar-nav navbar-right">
		
		                    <!-- Trang chủ -->
		                    <li class="active">
		                        <a href="Home.htm">
		                            <span class="glyphicon glyphicon-home"></span> 
		                            Trang chủ
		                        </a>
		                    </li>
		
		                    <!-- Dropdown -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                            <span class="glyphicon glyphicon-map-marker"></span>
		                            Ăn uống 
		                            <b class="caret"></b>
		                        </a>
		                        <ul class="dropdown-menu">
		                            <li><a href="#">Lẩu</a></li>
		                            <li><a href="#">Nướng</a></li>
		                            <li><a href="#">Hải sản</a></li>
		                            <li><a href="#">Món chay</a></li>
		                            <li><a href="#">Đặt tiệc</a></li>
		                        </ul>
		                    </li>
		
		                    <!-- ưu đãi -->
		                    <li><a href="#"><span class="glyphicon glyphicon-fire"></span>Ưu đãi hot</a></li>
		
		                    <!-- Ngôn ngữ -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                                <span class="glyphicon glyphicon-globe"></span>Ngôn ngữ<b class="caret"></b></a>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="#">Tiếng Việt</a></span></li>
		                                <li class="res"><span><a href="#">English</a></span></li>
		                        </ul>
		                    </li>
		
		                    <!-- Tài khoản -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>${mlogin}</a>
		                        <c:choose>
			                        <c:when test="${admin == false}">
			                        <ul class="dropdown-menu">
			                                <li class="login"><span><a href="login.htm">Trang cá nhân</a></span></li>
			                                <li class="res"><span><a href="logout.htm">Đăng xuất</a></span></li>
			                        </ul>
			                        </c:when>
			                        <c:otherwise>
			                        <ul class="dropdown-menu">
			                                <li class="login"><span><a href="login.htm">Trang cá nhân</a></span></li>
			                        		<li class="ad"><span><a href="tkNhaHang.htm">Quản lý</a></span></li>
			                                <li class="res"><span><a href="logout.htm">Đăng xuất</a></span></li>
			                        </ul>		
			                        </c:otherwise>
		                        </c:choose>
		                    </li> 
		                    <!-- /Tai Khoan -->
		                </ul>
		                <!-- /menu -->
		            </div><!-- /.navbar-collapse -->
		        </div> <!-- /container -->
		    </nav>    
		    <!-- end navigation 2 -->
	    </c:otherwise>
	</c:choose>
    <!-- END NAVIGATION -->

			<div class="container">
		        <div class="col-md-12 margin-b-30">
		            <div class="profile-edit">
		            <c:forEach var="run" items="${list }">
		                <form class="form-horizontal" action="updatePD.htm" method="post">
		                    <font color="red" size="5px">Phiếu Đặt</font>
		                    <fieldset>
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileFirstName">Mã Phiếu</label>
		                            
		                            <div class="col-md-8">
		                                <input name="maPD" value="${run.getMaPD() }" type="text" class="form-control" id="profileFirstName" readonly="">
		                            </div>
		                            
		                        </div>
		      
							    <div class="form-group">                    
									<label class="col-md-3 control-label" for="profileLastName">Ngày</label>
									<div id="datepicker" class="input-group date col-md-8" data-date-format="yyyy-mm-dd"> 
					                    <input class="form-control" readonly="" type="text" name="ngay_PD" value="${run.getNgay() }">
					                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> 
					                 </div>
							   </div>

		                        <div class="form-group">
		                        	<label class="col-md-3 control-label" for="profileAddress">Giờ</label>
		                        	<div class="col-md-8">
			                        <select id="txtGioDen" name="gio_PD" type="text" value="${run.getGio() }" class = "form-control" placeholder="Giờ" required="" autofocus="" readonly="" >
			                            <option>${run.getGio() }</option>
			                            <option>1:00</option>   <option>1:30</option>   <option>2:00</option>   <option>2:30</option>
			                            <option>3:00</option>   <option>3:30</option>   <option>4:00</option>   <option>4:30</option>
			                            <option>5:00</option>   <option>5:30</option>   <option>6:00</option>   <option>6:30</option>
			                            <option>7:00</option>   <option>7:30</option>   <option>8:00</option>   <option>8:30</option>
			                            <option>9:90</option>   <option>10:00</option>  <option>10:30</option>  <option>11:30</option>
			                            <option>12:30</option>  <option>13:00</option>  <option>13:30</option>  <option>14:00</option>
			                            <option>14:30</option>  <option>15:00</option>  <option>15:30</option>  <option>16:00</option>
			                            <option>16:30</option>  <option>17:00</option>  <option>17:30</option>  <option>18:00</option>
			                            <option>18:30</option>  <option>19:00</option>  <option>19:30</option>  <option>20:00</option>
			                            <option>20:30</option>  <option>21:00</option>  <option>21:30</option>  <option>22:00</option>
			                            <option>22:30</option>  <option>23:00</option>  <option>23:30</option>  <option>00:00</option>
			                        </select>
			                        </div>
		                        </div>
	
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Người lớn</label>
		                            <div class="col-md-8">
		                                <input name="nguoiLon_PD" value="${run.getNguoiLon()}" type="text" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		                        
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Trẻ em</label>
		                            <div class="col-md-8">
		                                <input name="treEm_PD" value="${run.getTreEm() }" type="text" class="form-control" id="profileCompany">
		                            </div>
		                        </div>

		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Mã NH</label>
		                            <div class="col-md-8">
		                                <input name="maNH_PD" value="${run.getNhhang().getMaNH() }" type="text" class="form-control" id="profileCompany" readonly="">
		                            </div>
		                        </div>
		                       
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Mã KH</label>
		                            <div class="col-md-8">
		                                <input name="maKH_PD" value="${run.getKhhang().getMaKH() }" type="text" class="form-control" id="profileCompany" readonly="">
		                            </div>
		                        </div>
		                        
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Ghi chú</label>
		                            <div class="col-md-8">
		                                <input name="ghiChu_PD" value="${run.getGhiChu() }" type="text" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		                        
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Giá</label>
		                            <div class="col-md-8">
		                                <input name="gia_PD" value="${run.getGia() }" type="text" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		
		                    </fieldset>
							
		                    <div class="panel-footer">
		                        <div class="row">
		                            <div class="col-md-9 col-md-offset-3">
		                                <button type="submit" class="btn btn-primary">Lưu Thông tin</button>
		                                <button type="reset" class="btn btn-default"><a href="tkPhieuDat.htm">Hủy</a></button>
		                            </div>
		                        </div>
		                    </div>
					
		                </form>
		                </c:forEach>
		            </div>
		        </div>
		    </div>
</body>
</html>