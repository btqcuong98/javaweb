<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <base href="${pageContext.servletContext.contextPath}/">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Thông tin cá nhân</title>

        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/waves.min.css" type="text/css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
        
        
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	    
	    <!-- gọi datepicker -->
    	<link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
	    
        <!-- viết css -->
        <link href="css/menu-light.css" type="text/css" rel="stylesheet">
       	<link href="css/style.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="bootstrap/Home.css">
		<link href="css/waves.min.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" href="bootstrap/Register.css">
		
		<!-- script jquery -->
        <script src="js/metisMenu.min.js"></script>
        <script src="js/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="js/pace.min.js"></script>
        <script src="js/waves.min.js"></script>
        <script src="js/jquery-jvectormap-world-mill-en.js"></script>
        <script type="text/javascript" src="js/custom.js"></script> 
        
        <!--page scripts-->
        <script src="js/data-tables/jquery.dataTables.js"></script>
        <script src="js/data-tables/dataTables.tableTools.js"></script>
        <script src="js/data-tables/dataTables.bootstrap.js"></script>
        <script src="js/data-tables/dataTables.responsive.js"></script>
        <script src="js/data-tables/tables-data.js"></script>
        
        <!-- Google Analytics:  -->
        <script>
            (function (i, s, o, g, r, a, m)
            {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function ()
                {
                    (i[r].q = i[r].q || []).push(arguments);
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '../../../../../www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-3560057-28', 'auto');
            ga('send', 'pageview');
        </script>
        
        <script type="text/javascript">
			$(function () {  
			$("#datepicker").datepicker({         
			autoclose: true,         
			todayHighlight: true 
			}).datepicker('update', ngayS);
			});
		</script>
        
        
    </head>
    <body>
		<!--NAVIGATION -->
	    <nav class="navbar navbar-default" role="navigation">
	        <div class="container">
	
	            <!-- header_logo -->
	            <div class="navbar-header">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	                    <a class="navbar-brand mr-auto" href="#"><img src="bootstrap/img/logo.png" style="width:55px;"></a>
	            </div>
	            <!-- /header_logo -->
	
	            <div class="collapse navbar-collapse navbar-ex1-collapse">
	
	                <!-- Tìm kiếm -->
	                <form class="navbar-form navbar-left" role="search">
	                    <div class="form-group">
	                        <input type="text" class="form-control" placeholder="Lẩu, Nướng, Hải sản, ....">
	                    </div>
	                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Tìm kiếm</button>
	                </form>
	                <!-- /Timkiem -->
	
	                <!-- menu -->
	                <ul class="nav navbar-nav navbar-right">
	
	                    <!-- Trang chủ -->
	                    <li class="active">
	                        <a href="Home.htm">
	                            <span class="glyphicon glyphicon-home"></span> 
	                            Trang chủ
	                        </a>
	                    </li>
	
	                    <!-- Dropdown -->
	                    <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                            <span class="glyphicon glyphicon-map-marker"></span>
	                            Ăn uống 
	                            <b class="caret"></b>
	                        </a>
	                        <ul class="dropdown-menu">
	                            <li><a href="#">Lẩu</a></li>
	                            <li><a href="#">Nướng</a></li>
	                            <li><a href="#">Hải sản</a></li>
	                            <li><a href="#">Món chay</a></li>
	                            <li><a href="#">Đặt tiệc</a></li>
	                        </ul>
	                    </li>
	
	                    <!-- ưu đãi -->
	                    <li><a href="#"><span class="glyphicon glyphicon-fire"></span>Ưu đãi hot</a></li>
	
	                    <!-- Ngôn ngữ -->
	                    <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                                                <span class="glyphicon glyphicon-globe"></span>Ngôn ngữ<b class="caret"></b></a>
	                        <ul class="dropdown-menu">
	                                <li class="login"><span><a href="#">Tiếng Việt</a></span></li>
	                                <li class="res"><span><a href="#">English</a></span></li>
	                        </ul>
	                    </li>
	
	                    <!-- Tài khoản -->
	                    <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>${mlogin}</a>
	                        <c:choose>
	                        <c:when test="${admin == false}">
	                        <ul class="dropdown-menu">
	                                <li class="login"><span><a href="trangCaNhan.htm">Trang cá nhân</a></span></li>
	                                <li class="res"><span><a href="logout.htm">Đăng xuất</a></span></li>
	                        </ul>
	                        </c:when>
	                        <c:otherwise>
	                        <ul class="dropdown-menu">
	                                <li class="login"><span><a href="trangCaNhan.htm">Trang cá nhân</a></span></li>
	                        		<li class="ad"><span><a href="tkNhaHang.htm">Quản lý</a></span></li>
	                                <li class="res"><span><a href="logout.htm">Đăng xuất</a></span></li>
	                        </ul>		
	                        </c:otherwise>
	                        </c:choose>
	                    </li> 
	                    <!-- /Tai Khoan -->
	                </ul>
	                <!-- /menu -->
	            </div><!-- /.navbar-collapse -->
	        </div> <!-- /container -->
	    </nav>    
   		<!-- END NAVIGATION -->

        <!-- Static navbar -->
	    <c:if test="${admin == true}">
	          <nav class="navbar-aside navbar-static-side" role="navigation">
	              <div class="sidebar-collapse nano">
	                  <div class="nano-content">
	                      <!-- câu quản lý -->
	                      <ul class="nav metismenu" id="side-menu">
	                          <!-- hình đại diện -->
	                          <li class="nav-header">
	                              <div class="dropdown side-profile text-left"> 
	                                  <span style="display: block;">
	                                      <img alt="image" class="img-circle" src="images/hinh_dai_dien.jpg" width="40">
	                                  </span>
	                                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">
	                                      <span class="clear" style="display: block;"> 
	                                          <span class="block m-t-xs"> 
	                                              <strong class="font-bold">${mlogin }<b class="caret"></b></strong>
	                                          </span>
	                                      </span> 
	                                  </a>
	                                  <ul class="dropdown-menu animated fadeInRight m-t-xs">
	                                      <li><a href="trangCaNhan.htm"><i class="fa fa-user"></i>Thông tin tài khoản</a></li>
	                                  </ul>
	                              </div>
	                          </li>
	                          <!-- end hình đại diện -->
	                          <li>
	                              <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">ĐẶT BÀN </span><span class="fa arrow"></span></a>
	                              <ul class="nav nav-second-level collapse">
	                                  <li><a href="tkPhieuDat.htm">Chi tiết </a></li>
	                              </ul>
	                          </li>
	                          <li>
	                              <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">MAILBOX </span><span class="fa arrow"></span></a>
	                              <ul class="nav nav-second-level collapse">
	                                  <li><a href="sendMail/${khDN}.htm">Gửi email</a></li>
	                              </ul>
	                          </li>
	                          
	                          <li class="active">
	                              <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">NHÀ HÀNG</span><span class="fa arrow"></span></a>
	                              <ul class="nav nav-second-level collapse">
	                                  <li><a href="tkNhaHang.htm">Chi tiết</a></li> 
	                              </ul>
	                          </li>
	                      
	                          <li>
                                <a href="#"><i class="fa fa-users"></i> <span class="nav-label">NGƯỜI DÙNG</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li><a href="tkKhachHang.htm">Danh sách khách hàng</a></li>
                                    <li><a href="tkAdmin.htm">Danh sách admin</a></li>
                                </ul>
                            </li>
	                      </ul>
	                      <!-- end cây quản lý -->
	                  </div>
	              </div>
	          </nav>
		</c:if>

        <!-- Trang cá nhân -->
        <div id="wrapper">
             <div class="content-wrapper container">
                 <div class="row">
                     <div class="col-sm-12">
                         <div class="page-title">
                             <h1>THÔNG TIN CÁ NHÂN <small></small></h1>
                             <ol class="breadcrumb">
                                 <li><a href="#"><i class="fa fa-home"></i></a></li>
                                 <li class="active">User Profile</li>
                             </ol>
                         </div>
                     </div>
                 </div><!-- end .page title-->

                 <!-- nội dung -->
                 <div class="col-md-4 margin-b-30">
                     <div class="profile-overview">
                         <div class="avtar text-center">
                             <img src="images/bautroi.jpg" alt="" class="img-thumbnail">
                             <hr>
                             <ul class="socials list-inline">
                                 <li><a ><i class="fa fa-facebook"></i></a></li>
                                 <li><a ><i class="fa fa-twitter"></i></a></li>
                                 <li><a ><i class="fa fa-google-plus"></i></a></li>
                                 <li><a ><i class="fa fa-linkedin"></i></a></li>
                                 <li><a ><i class="fa fa-github"></i></a></li>
                             </ul>
                             <hr>                               
                         </div>

                         <table class="table profile-detail table-condensed table-hover">
                             <thead>
                                 <tr>
                                     <th colspan="3">THÔNG TIN CÁ NHÂN</th>
                                 </tr>
                             </thead>
                             <c:forEach var="run" items="${list }">
                             <tbody>
                                 <tr>
                                     <td>Mã</td>
                                     <td><a>${run.getMaKH()}</a></td>
                                     <td><a  class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                 </tr>
                                 <tr>
                                     <td>Họ Tên</td>
                                     <td><a>${run.getHo()} ${run.getTen()}</a></td>
                                     <td><a  class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                 </tr>
                                 <tr>
                                     <td>Email</td>
                                     <td><a>${run.getEmail()}</a></td>
                                     <td><a  class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                 </tr>
                                 <tr>
                                     <td>Password</td>
                                     <td><a>********</a></td>
                                     <td><a  class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                 </tr>
                                 <tr>
                                     <td>Giới tính</td>
                                     <td><a>${run.getSex()}</a></td>
                                     <td><a  class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                 </tr>
                                 <tr>
                                     <td>Số điện thoại</td>
                                     <td><a>${run.getSdt() }</a></td>
                                     <td><a  class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                 </tr>
                                 <tr>
                                     <td>Ngày sinh</td>
                                     <td><a>${run.getNgaySinh()}</a></td>
                                     <td><a  class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                 </tr>
                                 <tr>
                                     <td>Chức vụ</td>
                                     <c:choose>
	                                     <c:when test="${admin==true}">
	                                     	<td><span class="label label-sm label-info">Administrator</span></td>
	                                     </c:when>
	                                     <c:otherwise>
	                                     	<td><span class="label label-sm label-info">Customer</span></td>
	                                     </c:otherwise>
                                     </c:choose>
                                     <td><a  class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                 </tr>
                             </tbody>
                             </c:forEach>
                         </table>
                     </div>
                 </div>



                 <div class="col-md-5 margin-b-30">
                     <div class="profile-edit">
                         <form class="form-horizontal" action="suaThongTinCaNhan.htm" method="post">
                             <h4 class="mb-xlg">CHỈNH SỬA THÔNG TIN</h4>
                             <c:forEach var="run" items="${list }">
                             <fieldset>
                                 <div class="form-group">
                                     <label class="col-md-3 control-label" for="profileFirstName" >Mã</label>
                                     <div class="col-md-8">
                                         <input type="text" name="ma" value="${run.getMaKH()}" class="form-control" id="profileFirstName" readonly="">
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <label class="col-md-3 control-label" for="profileLastName">Họ</label>
                                     <div class="col-md-8">
                                         <input type="text" name="ho" value="${run.getHo()}"class="form-control" id="profileLastName">
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <label class="col-md-3 control-label" for="profileLastName">Tên</label>
                                     <div class="col-md-8">
                                         <input type="text" name="ten" value="${run.getTen()}"class="form-control" id="profileLastName">
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <label class="col-md-3 control-label" for="profileAddress">Email</label>
                                     <div class="col-md-8">
                                         <input type="text" name="email" value="${run.getEmail()}" class="form-control" id="profileAddress">
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <label class="col-md-3 control-label" for="profileCompany">Giới tính</label>
                                     <div class="col-md-8">
                                         <input type="text" name="gioiTinh" value="${run.getSex()}" class="form-control" id="profileCompany">
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <label class="col-md-3 control-label" for="profileCompany">Số điện thoại</label>
                                     <div class="col-md-8">
                                     	<!-- //để hiển thị số điện thoại trùng và câu nhắc nhở || nếu k nó sẽ lấy sdt đúng trong database -->
                                     	<c:choose>
	                                     	<c:when test="${empty sodt }">
	                                     		<input type="text" name="sdt" value="${run.getSdt()}" class="form-control" id="profileCompany">
	                                     	</c:when>
	                                     	<c:otherwise>
		                                     	<font color="red" size="2px italiczed">${trungSDT }</font>
		                                     	<input type="text" name="sdt" value="${sodt}" class="form-control" id="profileCompany">
	                                     	</c:otherwise>
                                     	</c:choose>
                                         
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <label class="col-md-3 control-label" for="profileCompany">Ngày sinh</label>
                                     <div id="datepicker" class="col-md-8 input-group date" data-date-format="yyyy-mm-dd">
                                         <input type="text" name="ngaySinh" value="${run.getNgaySinh()}" class="form-control" id="profileCompany" readonly="">
                                     	 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> 
                                     </div>
                                 </div>

                             </fieldset>
							</c:forEach>
							
                             <h4 class="mb-xlg">Đổi Mật Khẩu</h4>
                             <fieldset class="mb-xl">
                                 <div class="form-group">
                                     <label class="col-md-3 control-label" for="profileNewPassword">Mật khẩu mới</label>
                                     <div class="col-md-8">
                                         <input type="password" name="pass" class="form-control" id="profileNewPassword">
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <label class="col-md-3 control-label" for="profileNewPasswordRepeat">Nhập lại</label>
                                     <div class="col-md-8">
                                     	<font color="red" size="2px italiczed">${thatbai }</font>
                                         <input type="password" name="repass"  class="form-control" id="profileNewPasswordRepeat">
                                     </div>
                                 </div>
                             </fieldset>
                             <div class="panel-footer">
                                 <div class="row">
                                     <div class="col-md-9 col-md-offset-3">
                                         <button type="submit" class="btn btn-primary">Lưu thông tin</button>
                                         <button type="reset" class="btn btn-default"><a href="trangCaNhan.htm">Hủy</a></button>
                                     </div>
                                 </div>
                             </div>

                         </form>
                     </div>
                 </div>
                 <div class="col-md-3">
                     <div class="profile-states">
                         <h3>Author</h3>
                         <div class="sale-state-box">
                             <h3>N16DCCN015</h3>
                             <span>Bùi Thái Quốc Cường</span>
                         </div>
                         <div class="sale-state-box">
                             <h3>N16CQCP01</h3>
                             <span>Lớp</span>
                         </div>
                         <div class="sale-state-box">
                             <h3>QL Đặt bàn</h3>
                             <span>15</span>
                         </div>
                     </div>
                 </div>
             </div> 
         </div>
   
     </section>


		
        
    </body>

<!-- Mirrored from themeforestdemo.com/templates/shapebootstrap/html/be-admin/products.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 18 May 2017 03:26:24 GMT -->
</html>