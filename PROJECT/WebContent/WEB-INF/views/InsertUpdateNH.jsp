<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <base href="${pageContext.servletContext.contextPath}/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="bootstrap/addInsert.css">
    <link rel="stylesheet" href="bootstrap/Home.css">
</head>
<body>
	<!--NAVIGATION -->
    <c:choose>
		<c:when test="${empty mlogin}"> 
		<!-- navigation 1-->
		<nav class="navbar navbar-default" role="navigation">
		        <div class="container">
		            <!-- header_logo -->
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                    <a class="navbar-brand mr-auto" href="#"><img src="bootstrap/img/logo.png" style="width:55px;"></a>
		            </div>
		            <!-- /header_logo -->
		
		            <div class="collapse navbar-collapse navbar-ex1-collapse">
		
		                <!-- Tìm kiếm -->
		                <form class="navbar-form navbar-left" role="search">
		                    <div class="form-group">
		                        <input type="text" class="form-control" placeholder="Lẩu, Nướng, Hải sản, ....">
		                    </div>
		                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Tìm kiếm</button>
		                </form>
		                <!-- /Timkiem -->
		
		                <!-- menu -->
		                <ul class="nav navbar-nav navbar-right">
		
		                    <!-- Trang chủ -->
		                    <li class="active">
		                        <a href="Home.htm">
		                            <span class="glyphicon glyphicon-home"></span> 
		                            Trang chủ
		                        </a>
		                    </li>
		
		                    <!-- Dropdown -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                            <span class="glyphicon glyphicon-map-marker"></span>
		                            Ăn uống 
		                            <b class="caret"></b>
		                        </a>
		                        <ul class="dropdown-menu">
		                            <li><a href="#">Lẩu</a></li>
		                            <li><a href="#">Nướng</a></li>
		                            <li><a href="#">Hải sản</a></li>
		                            <li><a href="#">Món chay</a></li>
		                            <li><a href="#">Đặt tiệc</a></li>
		                        </ul>
		                    </li>
		
		                    <!-- ưu đãi -->
		                    <li><a href="#"><span class="glyphicon glyphicon-fire"></span>Ưu đãi hot</a></li>
		
		                    <!-- Ngôn ngữ -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                                <span class="glyphicon glyphicon-globe"></span>Ngôn ngữ<b class="caret"></b></a>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="#">Tiếng Việt</a></span></li>
		                                <li class="res"><span><a href="#">English</a></span></li>
		                        </ul>
		                    </li>
		
		                    <!-- Tài khoản -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>Tài khoản</a>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="login.htm">Đăng nhập</a></span></li>
		                                <li class="res"><span><a href="register.htm">Đăng ký</a></span></li>
		                        </ul>
		                    </li> 
		                    <!-- /Tai Khoan -->
		
		                </ul>
		                <!-- /menu -->
		            </div><!-- /.navbar-collapse -->
		        </div> <!-- /container -->
		    </nav>
		    <!-- end navigation 1 -->
		</c:when>
		
		
		
	    <c:otherwise>  
	    	<!-- navigation 2-->
		    <nav class="navbar navbar-default" role="navigation">
		        <div class="container">
		
		            <!-- header_logo -->
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                    <a class="navbar-brand mr-auto" href="#"><img src="bootstrap/img/logo.png" style="width:55px;"></a>
		            </div>
		            <!-- /header_logo -->
		
		            <div class="collapse navbar-collapse navbar-ex1-collapse">
		
		                <!-- Tìm kiếm -->
		                <form class="navbar-form navbar-left" role="search">
		                    <div class="form-group">
		                        <input type="text" class="form-control" placeholder="Lẩu, Nướng, Hải sản, ....">
		                    </div>
		                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Tìm kiếm</button>
		                </form>
		                <!-- /Timkiem -->
		
		                <!-- menu -->
		                <ul class="nav navbar-nav navbar-right">
		
		                    <!-- Trang chủ -->
		                    <li class="active">
		                        <a href="Home.htm">
		                            <span class="glyphicon glyphicon-home"></span> 
		                            Trang chủ
		                        </a>
		                    </li>
		
		                    <!-- Dropdown -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                            <span class="glyphicon glyphicon-map-marker"></span>
		                            Ăn uống 
		                            <b class="caret"></b>
		                        </a>
		                        <ul class="dropdown-menu">
		                            <li><a href="#">Lẩu</a></li>
		                            <li><a href="#">Nướng</a></li>
		                            <li><a href="#">Hải sản</a></li>
		                            <li><a href="#">Món chay</a></li>
		                            <li><a href="#">Đặt tiệc</a></li>
		                        </ul>
		                    </li>
		
		                    <!-- ưu đãi -->
		                    <li><a href="#"><span class="glyphicon glyphicon-fire"></span>Ưu đãi hot</a></li>
		
		                    <!-- Ngôn ngữ -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                                <span class="glyphicon glyphicon-globe"></span>Ngôn ngữ<b class="caret"></b></a>
		                        <ul class="dropdown-menu">
		                                <li class="login"><span><a href="#">Tiếng Việt</a></span></li>
		                                <li class="res"><span><a href="#">English</a></span></li>
		                        </ul>
		                    </li>
		
		                    <!-- Tài khoản -->
		                    <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>${mlogin}</a>
		                        <c:choose>
			                        <c:when test="${admin == false}">
			                        <ul class="dropdown-menu">
			                                <li class="login"><span><a href="login.htm">Trang cá nhân</a></span></li>
			                                <li class="res"><span><a href="logout.htm">Đăng xuất</a></span></li>
			                        </ul>
			                        </c:when>
			                        <c:otherwise>
			                        <ul class="dropdown-menu">
			                                <li class="login"><span><a href="login.htm">Trang cá nhân</a></span></li>
			                        		<li class="ad"><span><a href="tkNhaHang.htm">Quản lý</a></span></li>
			                                <li class="res"><span><a href="logout.htm">Đăng xuất</a></span></li>
			                        </ul>		
			                        </c:otherwise>
		                        </c:choose>
		                    </li> 
		                    <!-- /Tai Khoan -->
		                </ul>
		                <!-- /menu -->
		            </div><!-- /.navbar-collapse -->
		        </div> <!-- /container -->
		    </nav>    
		    <!-- end navigation 2 -->
	    </c:otherwise>
	</c:choose>
    <!-- END NAVIGATION -->

	<c:choose>
		<c:when test="${flag == 0}">>
			<div class="container">
		        <div class="col-md-12 margin-b-30">
		            <div class="profile-edit">
		                <form class="form-horizontal" action="addNH.htm" method="post" enctype="multipart/form-data">
		                    <font color="red" size="5px">Nhà Hàng</font>
		                    <fieldset>
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileFirstName">Mã Nhà Hàng</label>
		                            <div class="col-md-8">
		                                <input name="maNH" type="text" class="form-control" id="profileFirstName" readonly="">
		                            </div>
		                        </div>
		                        
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileLastName">Tên Nhà Hàng</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${tenRong}</font>
		                                <input name="tenNH" type="text" class="form-control" id="profileLastName">
		                            </div>
		                        </div>
		                    </fieldset>
		                    <hr class="dotted tall">
		                    <font color="red" size="5px">Chi tiết Nhà Hàng</font>
		                    <fieldset>
		                    	
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileAddress">Thời gian phục vụ</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${tgRong}</font>
		                                <input name="thoiGian" type="text" class="form-control" id="profileAddress">
		                            </div>
		                        </div>
		                        
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Khoảng giá</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${giaRong}</font>
		                                <input name="gia" type="text" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		                        
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Địa chỉ</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${dcRong}</font>
		                                <input name="diaChi" type="text" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		                    </fieldset>
		                    <hr class="dotted tall">
		                    <font color="red" size="5px">Thông tin khác</font>
		                    <fieldset>
		                    	
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Trạng thái</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${ttRong}</font>
		                                <input name="trangThai" type="text" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		                       
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Điện thoại</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${dtRong}</font>
		                                <input name="dienThoai" type="text" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		                        
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Url</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${urlRong}</font>
		                                <input name="url" type="file" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		
		                    </fieldset>
		
		                    <div class="panel-footer">
		                        <div class="row">
		                            <div class="col-md-9 col-md-offset-3">
		                                <button type="submit" class="btn btn-primary">Lưu Thông tin</button>
		                                <button type="reset" class="btn btn-default"><a href="tkNhaHang.htm">Hủy</a></button>
		                            </div>
		                        </div>
		                    </div>
		
		                </form>
		            </div>
		        </div>
		    </div>
		</c:when>
		
		<c:otherwise>
			<div class="container">
		        <div class="col-md-12 margin-b-30">
		            <div class="profile-edit">
		            <c:forEach var="run" items="${list }">
		                <form class="form-horizontal" action="updateNH.htm" method="post" enctype="multipart/form-data">
		                    <font color="red" size="5px">Nhà Hàng</font>
		                    <fieldset>
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileFirstName">Mã Nhà Hàng</label>
		                            
		                            <div class="col-md-8">
		                                <input name="maNH" value="${run.getMaNH() }" type="text" class="form-control" id="profileFirstName" readonly="">
		                            </div>
		                            
		                        </div>
		                        
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileLastName">Tên Nhà Hàng</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${tenRong}</font>
		                                <input name="tenNH" value="${run.getTenNH() }" type="text" class="form-control" id="profileLastName">
		                            </div>
		                        </div>
		                    </fieldset>
		                    <hr class="dotted tall">
		                    <font color="red" size="5px">Chi tiết Nhà Hàng</font>
		                    <fieldset>
		                    	
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileAddress">Thời gian phục vụ</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${tgRong}</font>
		                                <input name="thoiGian" type="text" value="${run.getThoiGianPhucVu() }" class="form-control" id="profileAddress">
		                            </div>
		                        </div>
		                        
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Khoảng giá</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${giaRong}</font>
		                                <input name="gia" value="${run.getKhoangGia()}" type="text" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		                        
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Địa chỉ</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${dcRong}</font>
		                                <input name="diaChi" value="${run.getDiaChi() }" type="text" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		                    </fieldset>
		                    <hr class="dotted tall">
		                    <font color="red" size="5px">Thông tin khác</font>
		                    <fieldset>
		                    	
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Trạng thái</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${ttRong}</font>
		                                <input name="trangThai" value="${run.getTrangThai() }" type="text" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		                       
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Điện thoại</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${dtRong}</font>
		                                <input name="dienThoai" value="${run.getDienThoai() }" type="text" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		                        
		                        <div class="form-group">
		                            <label class="col-md-3 control-label" for="profileCompany">Url</label>
		                            <div class="col-md-8">
		                            	<font color="red" size="1px italicized">${urlRong}</font>
		                                <input name="url" value="${run.getUrlHinhAnh() }" type="file" class="form-control" id="profileCompany">
		                            </div>
		                        </div>
		
		                    </fieldset>
							
		                    <div class="panel-footer">
		                        <div class="row">
		                            <div class="col-md-9 col-md-offset-3">
		                                <button type="submit" class="btn btn-primary">Lưu Thông tin</button>
		                                <button type="reset" class="btn btn-default"><a href="tkNhaHang.htm">Hủy</a></button>
		                            </div>
		                        </div>
		                    </div>
					
		                </form>
		                </c:forEach>
		            </div>
		        </div>
		    </div>
		</c:otherwise>
	</c:choose>
</body>
</html>