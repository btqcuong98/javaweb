package ptithcm.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ptithcm.entity.KhachHang;
//import ptithcm.entity.User;

@Transactional
@Controller
public class RegisterController {

	@Autowired
	SessionFactory factory;
	
	List<KhachHang> list;
	//GET DATA FROM DATABASE
		public List<KhachHang> listKhachHang(){
			Session session  = factory.getCurrentSession();
			String hql = "FROM KhachHang";

			Query query = session.createQuery(hql);
			list = query.list();
			return list;
		}
		
	@RequestMapping(value="register", method = RequestMethod.GET)
	public String insert(ModelMap model){
		model.addAttribute("kh", new KhachHang());
		return "Register";
	}
	
	@RequestMapping(value="register", method = RequestMethod.POST)
	public String insert(ModelMap model, 
						@RequestParam("firstname") String firstname,
						@RequestParam("lastname") String lastname,
						@RequestParam("email") String email,
						@RequestParam("sdt") String sdt,
						@RequestParam("password") String password,
						@RequestParam("repassword") String repassword,
						@RequestParam("sex") String sex,
						@RequestParam("date") String date){
		if(sdt.length()<5){
			model.addAttribute("errors","* Số điện thoại này không tồn tại");
			return "Register";
		}
		
		if(!password.equals(repassword)){
			model.addAttribute("repass","* Xác nhận mật khẩu thất bại");
			return "Register";
		}
		

		
		KhachHang kh = new KhachHang();
		kh.setHo(firstname);
		kh.setTen(lastname);
		kh.setEmail(email);
		kh.setSdt(sdt);
		kh.setPassword(password);
		kh.setSex(sex);
		kh.setAdmin(false);
		kh.setNgaySinh(date);
		
		Session sesion = factory.openSession();
		Transaction t = sesion.beginTransaction();
		String hql = "FROM KhachHang kh WHERE kh.Sdt = :sdt";
		Query query = sesion.createQuery(hql);
		query.setParameter("sdt", sdt);
		List<KhachHang> list = query.list();
		
		if(!list.isEmpty()){
			model.addAttribute("errors","* Số điện thoại này đã được sử dụng");
			return "Register";
		}
		
		else{
			try {
				sesion.save(kh);
				t.commit();
				return "redirect:/login.htm";
			} catch (Exception e) {
				t.rollback();
			}finally {
				sesion.close();
			}
		}

		return "Register";
	}
}
