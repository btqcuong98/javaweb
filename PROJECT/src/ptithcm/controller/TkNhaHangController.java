package ptithcm.controller;

import java.io.File;
import java.util.List;

import javax.mail.Multipart;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import ptithcm.entity.NhaHang;

@Transactional
@Controller
public class TkNhaHangController {
	@Autowired SessionFactory factory;
	@Autowired ServletContext context;
	
	//FROM USER
	public List<NhaHang> listNH(){
		Session sessionCurr  = factory.getCurrentSession();
		
		String hql = "FROM NhaHang";
		Query query = sessionCurr.createQuery(hql);
		List<NhaHang> listNH = query.list();
		return listNH;
	}
	
	//INDEX
	@RequestMapping("tkNhaHang")
	public String tkNhaHang(ModelMap model){
		model.addAttribute("listNH", listNH());
		return "TkNhaHang";
	}
	
	//XÓA
	@RequestMapping(value="deleteNH/{manh}")
	public String delete(ModelMap model, @PathVariable("manh") int manh){

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        
        String hql = "Delete from NhaHang nh where nh.MaNH= :id";
        Query query = session.createQuery(hql);
        
        try {
        	 query.setParameter("id", manh).executeUpdate();
             transaction.commit();
		} catch (Exception e) {
			return "redirect:/tkNhaHang.htm";
		}
       
        
        session.clear();
        session.close();
        
        return "redirect:/tkNhaHang.htm";
	}
	
	//THÊM
	@RequestMapping(value="addNH", method=RequestMethod.GET)
	public String add (ModelMap model){
		model.addAttribute("flag",0); //0: thêm || 1: sửa
		return "InsertUpdateNH";
	}
	
	@RequestMapping(value="addNH", method=RequestMethod.POST)
	public String add(ModelMap model, HttpSession ss,
						@RequestParam("tenNH") String tenNH,
						@RequestParam("dienThoai") String dienThoai,
						@RequestParam("gia") String gia,
						@RequestParam("thoiGian") String thoiGian,
						@RequestParam("url") MultipartFile url,
						@RequestParam("diaChi") String diaChi,
						@RequestParam("trangThai") String trangThai){
		//KIỂM TRA VALIDATION
		if(tenNH.trim().length()==0){
			ss.setAttribute("tenRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("tenRong");
		
		if(thoiGian.trim().length()==0){
			ss.setAttribute("tgRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("tgRong");
		
		if(gia.trim().length()==0){
			ss.setAttribute("giaRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("giaRong");
		
		if(diaChi.trim().length()==0){
			ss.setAttribute("dcRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("dcRong");
		
		if(trangThai.trim().length()==0){
			ss.setAttribute("ttRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("ttRong");
		
		if(dienThoai.trim().length()==0){
			ss.setAttribute("dtRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("dtRong");
	
		if(url.isEmpty()){
			ss.setAttribute("urlRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("urlRong");
	
		//INSERT
		Session session = factory.openSession();
		Transaction t = session.beginTransaction();
		
		NhaHang nh = new NhaHang();
		nh.setTenNH(tenNH);
		nh.setDienThoai(dienThoai);
		nh.setKhoangGia(gia);
		nh.setThoiGianPhucVu(thoiGian);
		try {
			String photoPath = context.getRealPath("files/" + url.getOriginalFilename());
			url.transferTo(new File(photoPath));
			nh.setUrlHinhAnh(url.getOriginalFilename());
		} catch (Exception e) {
		}
		
		nh.setDiaChi(diaChi);
		nh.setTrangThai(trangThai);
		
		try {
			session.save(nh);
			t.commit();
		} catch (Exception e) {
			
		}finally {
			session.close();
		}
		
		
		return "redirect:/tkNhaHang.htm";
	}
	
	//SỬA
	@RequestMapping(value="updateNH/{manh}", method=RequestMethod.GET)
	public String edit (ModelMap model, @PathVariable("manh") int manh){
		
		Session sessionGet = factory.getCurrentSession();
		String hqlGet = "FROM NhaHang nh WHERE nh.MaNH = :id";
		Query query = sessionGet.createQuery(hqlGet);
		query.setParameter("id", manh);
		List<NhaHang> list = query.list();
		
		int flag = list.get(0).getMaNH();
		model.addAttribute("flag", flag);
		model.addAttribute("list", list);
		return "InsertUpdateNH";
	}
	
	@RequestMapping(value="updateNH", method=RequestMethod.POST)
	public String edit (ModelMap model, HttpSession ss,
										@RequestParam("maNH") int maNH,
										@RequestParam("tenNH") String tenNH,
										@RequestParam("dienThoai") String dienThoai,
										@RequestParam("gia") String gia,
										@RequestParam("thoiGian") String thoiGian,
										@RequestParam("url") MultipartFile url,
										@RequestParam("diaChi") String diaChi,
										@RequestParam("trangThai") String trangThai){
									
		//KIỂM TRA VALIDATION
		if(tenNH.trim().length()==0){
			ss.setAttribute("tenRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("tenRong");
		
		if(thoiGian.trim().length()==0){
			ss.setAttribute("tgRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("tgRong");
		
		if(gia.trim().length()==0){
			ss.setAttribute("giaRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("giaRong");
		
		if(diaChi.trim().length()==0){
			ss.setAttribute("dcRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("dcRong");
		
		if(trangThai.trim().length()==0){
			ss.setAttribute("ttRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("ttRong");
		
		if(dienThoai.trim().length()==0){
			ss.setAttribute("dtRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("dtRong");
	
		if(url.isEmpty()){
			ss.setAttribute("urlRong", "* Không được để trống");
			return "InsertUpdateNH";
		}
		ss.removeAttribute("urlRong");
		
		//UPDATE
		Session session = factory.openSession();
		Transaction t = session.beginTransaction();
		
		NhaHang nh = new NhaHang();
		nh.setMaNH(maNH);
		nh.setTenNH(tenNH);
		nh.setDienThoai(dienThoai);
		nh.setKhoangGia(gia);
		nh.setThoiGianPhucVu(thoiGian);
		try {
			String photoPath = context.getRealPath("files/" + url.getOriginalFilename());
			url.transferTo(new File(photoPath));
			nh.setUrlHinhAnh(url.getOriginalFilename());
		} catch (Exception e) {
		}
		nh.setDiaChi(diaChi);
		nh.setTrangThai(trangThai);
		
		try {
			session.update(nh);
			t.commit();
		} catch (Exception e) {
			t.rollback();
		}finally {
			session.close();
		}
		
		
		return "redirect:/tkNhaHang.htm";

	}
	
}
