package ptithcm.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ptithcm.entity.KhachHang;
import ptithcm.entity.NhaHang;
import ptithcm.entity.PhieuDat;



@Transactional
@Controller
public class TkPhieuDatController {
	@Autowired
	SessionFactory factory;
	
	//FROM PHIEUDAT
	public List<PhieuDat> listPD(){
		Session sessionCurr  = factory.getCurrentSession();
		
		String hql = "FROM PhieuDat";
		Query query = sessionCurr.createQuery(hql);
		List<PhieuDat> listPD = query.list();
		return listPD;
	}
	
	//INDEX
	@RequestMapping("tkPhieuDat")
	public String tkDatBan(ModelMap model){
		model.addAttribute("listPD", listPD());
		return "TkPhieuDat";
	}
		
	//XÓA
	@RequestMapping(value="deletePD/{mapd}")
	public String delete(ModelMap model, @PathVariable("mapd") int mapd){

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        
        String hql = "Delete from PhieuDat pd where pd.MaPD= :id";
        Query query = session.createQuery(hql);
        query.setParameter("id", mapd).executeUpdate();
        transaction.commit();
        
        session.clear();
        session.close();
        
        return "redirect:/tkPhieuDat.htm";
	}
	
	//SỬA
	@RequestMapping(value="updatePD/{mapd}", method=RequestMethod.GET)
	public String edit (ModelMap model, @PathVariable("mapd") int mapd){
		
		Session sessionGet = factory.getCurrentSession();
		String hqlGet = "FROM PhieuDat pd WHERE pd.MaPD = :id";
		Query query = sessionGet.createQuery(hqlGet);
		query.setParameter("id", mapd);
		List<PhieuDat> list = query.list();
		
		model.addAttribute("ngayD", list.get(0).getNgay());
		model.addAttribute("list", list);
		return "UpdatePD";
	}
	
	@RequestMapping(value="updatePD", method=RequestMethod.POST)
	public String edit (ModelMap model, 
										@RequestParam("maPD") int maPD,
										@RequestParam("ngay_PD") String ngay_PD,
										@RequestParam("gio_PD") String gio_PD,
										@RequestParam("nguoiLon_PD") String nguoiLon_PD,
										@RequestParam("treEm_PD") String treEm_PD,
										@RequestParam("maNH_PD") int maNH_PD,
										@RequestParam("maKH_PD") int maKH_PD,
										@RequestParam("ghiChu_PD") String ghiChu_PD,
										@RequestParam("gia_PD") Float gia_PD){
		//UPDATE
		Session session = factory.openSession();
		Transaction t = session.beginTransaction();
		
		Session sessionCurr  = factory.getCurrentSession();
		String hqlNH = "FROM NhaHang nh WHERE nh.MaNH = :id";
		Query queryNH = sessionCurr.createQuery(hqlNH);
		queryNH.setParameter("id", maNH_PD);
		List<NhaHang> listNH = queryNH.list();
		
		String hqlKH = "FROM KhachHang kh WHERE kh.MaKH = :id";
		Query queryKH = sessionCurr.createQuery(hqlKH);
		queryKH.setParameter("id", maKH_PD);
		List<KhachHang> listKH = queryKH.list();
		
		
		PhieuDat pd = new PhieuDat();
		pd.setMaPD(maPD);
		pd.setNgay(ngay_PD);
		pd.setGio(gio_PD);
		pd.setNguoiLon(nguoiLon_PD);
		pd.setTreEm(treEm_PD);
		pd.setNhhang(listNH.get(0));
		pd.setKhhang(listKH.get(0));
		pd.setGhiChu(ghiChu_PD);
		pd.setGia(gia_PD);
		
		try {
			session.update(pd);
			t.commit();
		} catch (Exception e) {
			t.rollback();
			System.out.print(e);
		}finally {
			session.close();
		}
		
		
		return "redirect:/tkPhieuDat.htm";

	}
}
