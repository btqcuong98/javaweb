package ptithcm.controller;


import java.util.List;

import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Transactional
@Controller
public class SendMailController {
	@Autowired	SessionFactory factory;
	@Autowired	JavaMailSender mailer;
	

	@RequestMapping("sendMail/{id}")
	public String MailBox(ModelMap model, @PathVariable("id") int maKH){
		
		String text = "Do nhân viên không liên lạc được với quý khách nên bàn đặt đã được hủy! \nMong gặp lại quý khách vào lần sau!";
		String subject = "Trình trạng đặt bàn";
		
		Session sessionCurr = factory.getCurrentSession();
		String hql = "SELECT kh.Email FROM KhachHang kh WHERE kh.MaKH = '"+maKH+"'";
		Query query = sessionCurr.createQuery(hql);
		List<String> listMail = query.list();
		
		model.addAttribute("to", listMail.get(0));
		model.addAttribute("subject", subject);
		model.addAttribute("text", text);

		return "MailCompose";
	}
	
	@RequestMapping(value="sendMail", method = RequestMethod.POST)
	public String SendMail(	ModelMap model
							,@RequestParam("to") String to
							,@RequestParam("subject") String subject
							,@RequestParam("text") String text){

		try {
			String from = "DatBanOnline@gmail.com";
			

			MimeMessage mail = mailer.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mail);
			helper.setFrom(from, from);
			helper.setTo(to);
			helper.setReplyTo(from, from);
			helper.setSubject(subject);
			helper.setText(text, true);
			mailer.send(mail);
		
		} catch (Exception e) {
		}
		return "redirect:/tkPhieuDat.htm";
	}
}
