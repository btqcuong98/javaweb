package ptithcm.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

//import ptithcm.entity.DatBan;
import ptithcm.entity.KhachHang;
import ptithcm.entity.NhaHang;
import ptithcm.entity.PhieuDat;
import ptithcm.entity.ThongTin;

@Transactional
@Controller

public class DatBanController {
	@Autowired
	SessionFactory factory;
	public int MaNH;
	//INDEX
		@RequestMapping("muahang/{id}")
		public String muahang(ModelMap model, @PathVariable("id") int id){
	
			Session sesion = factory.openSession();
			Transaction t = sesion.beginTransaction();
			
			MaNH = id;
			
			String hql = "FROM NhaHang nh WHERE nh.MaNH = :id";
			Query query = sesion.createQuery(hql);
			query.setParameter("id", id);
			List<NhaHang> list = query.list();
			
			String hql_ha = "FROM ThongTin tt WHERE tt.nhahang.MaNH = :id and tt.url LIKE '%ig%'";
			Query query_ha = sesion.createQuery(hql_ha);
			query_ha.setParameter("id", id);
			List<ThongTin> listHA = query_ha.list();
			
			String hql_mn = "FROM ThongTin tt WHERE tt.nhahang.MaNH = :id and tt.url LIKE '%mn%'";
			Query query_mn = sesion.createQuery(hql_mn);
			query_mn.setParameter("id", id);
			List<ThongTin> listMN = query_mn.list();
			
			model.addAttribute("list", list);
			model.addAttribute("listHA", listHA);
			model.addAttribute("listMN", listMN);
			
			return "MuaHang";
		}

		@RequestMapping("thanhcong")
		public String mua(ModelMap model, HttpSession ss,
										@RequestParam("date") String date,
										@RequestParam("hour") String hour,
										@RequestParam("nguoiLon") String nguoiLon,
										@RequestParam("treEm") String treEm,
										@RequestParam("note") String note){
			
			
			try {
				String ngayDatBan = date + " " + hour;
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");  
				Date strToDate = formatter.parse(ngayDatBan);  

			    Date ngayHT = java.util.Calendar.getInstance().getTime();    
			    
			    
			    long getDiff = strToDate.getTime() - ngayHT.getTime();
			    long getDaysDiff = TimeUnit.MILLISECONDS.toMinutes(getDiff);

			    if(getDaysDiff < 30){
			    	ss.setAttribute("thoiGianHopLe", "NO");
			    	System.out.println("khoảng cách: "+"NO");  
			    	System.out.println("khoảng cách: "+getDaysDiff);
			    	
			    	model.addAttribute("MaNH", MaNH);
			    	model.addAttribute("thongbao","QUÝ KHÁCH VUI LÒNG ĐẶT CHỖ 30 PHÚT TRƯỚC KHI ĐẾN !");
			    	model.addAttribute("ghichu", "");
			    	/*return "redirect:/muahang/"+MaNH+".htm";*/
			    	return "ThongBao";
			    }

			} catch (Exception e) {
				System.out.println("loi: "+e);  
			}
		    
		    ss.removeAttribute("thoiGianHopLe");
		    
			//set data0
			PhieuDat dat = new PhieuDat();
			dat.setNgay(date);
			dat.setGio(hour);
			dat.setNguoiLon(nguoiLon);
			dat.setTreEm(treEm);
			
			if(note.trim().length()==0){
				dat.setGhiChu("x");
			}else{
				dat.setGhiChu(note);
			}
			dat.setGia((float) 0.0);
			
			
			
			Session sessionCurr  = factory.getCurrentSession();
			
			String hql = "FROM NhaHang nh WHERE nh.MaNH = :MaNH";
			Query query = sessionCurr.createQuery(hql);
			query.setParameter("MaNH", MaNH);
			List<NhaHang> list = query.list();
			dat.setNhhang(list.get(0));
			
			String hql2 = "FROM KhachHang kh WHERE kh.MaKH = :MaKH";
			Query query2 = sessionCurr.createQuery(hql2);
			query2.setParameter("MaKH", LoginController.khDN);
			List<KhachHang> list2 = query2.list();
			dat.setKhhang(list2.get(0));

			
			Session sesion = factory.openSession();
			Transaction t = sesion.beginTransaction();
			
			try {
				sesion.save(dat);
				t.commit();
				
				model.addAttribute("MaNH", MaNH);
		    	model.addAttribute("thongbao","NHÂN VIÊN SẼ GỌI CHO QUÝ KHÁCH TRONG VÀI PHÚT TỚI, VUI LÒNG GIỮ LIÊN LẠC VỚI CHÚNG TÔI!");
		    	model.addAttribute("ghichu", "* Đặt bàn thành công chỉ khi có xác nhận từ nhân viên");
				return "ThongBao";
				
			} catch (Exception e) {
				System.out.print(e);
				t.rollback();
				
			}finally {
				sesion.close();
			}
			return "MuaHang";
		}
}
