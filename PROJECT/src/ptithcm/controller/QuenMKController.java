package ptithcm.controller;

import java.util.List;
import java.util.Random;

import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ptithcm.entity.KhachHang;

@Transactional
@Controller
public class QuenMKController {
	@Autowired
	SessionFactory factory;
	@Autowired
	JavaMailSender mailer;
	
	@RequestMapping("quenMK")
	public String QuenMK(){
		return "QuenMK";
	}
	
	@RequestMapping("newPass")
	public String NewPass(ModelMap model 
										,@RequestParam("newpassSDT") String sdt
										,@RequestParam("newpassMail") String gmail){
		Session sessionCurr = factory.getCurrentSession();
		String hql = "FROM KhachHang kh WHERE kh.Sdt = '"+sdt+"'";
		Query query = sessionCurr.createQuery(hql);
		List<KhachHang> list = query.list();
		
		if(list.isEmpty()){
			model.addAttribute("newpassSDT", "* Số điện thoại không tồn tại trong hệ thống");
			return "QuenMK";
		}
		
		if(!list.get(0).getEmail().equals(gmail)){
			model.addAttribute("newpassMail", "* Mail không đúng");
			return "QuenMK";
		}
		
		
		Random rd = new Random();
		int number = 100000 + rd.nextInt(888888); 
		
		Session sessionOpen = factory.openSession();
		Transaction t = sessionOpen.beginTransaction();
		try {
			String from = "DatBanOnline@gmail.com";
			String text = "Mật khẩu mới của bạn là: "+number+" . Link truy cập: "+"http://localhost:9999/PROJECT/login.htm";

			MimeMessage mail = mailer.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mail);
			helper.setFrom(from, from);
			helper.setTo(gmail);
			helper.setReplyTo(from, from);
			helper.setSubject("Reset Password");
			helper.setText(text, true);
			mailer.send(mail);
			
			//đổi trong db
			String passNew = Integer.toString(number);
			list.get(0).setPassword(passNew);

			sessionOpen.update(list.get(0));
			t.commit();
		
		} catch (Exception e) {
			t.rollback();
		}
		return "redirect:/login.htm";
	}
}
