package ptithcm.controller;


import java.util.List;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ptithcm.entity.KhachHang;
import ptithcm.entity.NhaHang;


@Transactional
@Controller
public class HomeController {
	@Autowired
	SessionFactory factory;
	
	public static String sodt="";
	
	//FROM USER
	List<NhaHang> listNH;
	public List<NhaHang> listNH(){
		Session session  = factory.getCurrentSession();
		String hql = "FROM NhaHang";

		Query query = session.createQuery(hql);
		listNH = query.list();
		return listNH;
	}
	
	//INDEX
	@RequestMapping("Home")
	public String home(ModelMap model){
		model.addAttribute("restaurants", listNH());
		return "Home";
	}
	
	//TRANG CÁ NHÂN
	@RequestMapping("trangCaNhan")
	public String profile(ModelMap model){
		
		Session sessionCurr = factory.getCurrentSession();
		String hql = "FROM KhachHang kh WHERE kh.MaKH = :id";
		Query query = sessionCurr.createQuery(hql);
		query.setParameter("id", LoginController.khDN);
		List<KhachHang> list = query.list();
		
		model.addAttribute("list", list);
		model.addAttribute("ngayS", list.get(0).getNgaySinh()); // bỏ vào date picker , nếu k sẽ lấy ngày hiện tại 
		return "TrangCaNhan";
	}
	
	//SỬA TT CÁ NHÂN
	@RequestMapping(value="suaThongTinCaNhan", method=RequestMethod.POST)
	public String edit (ModelMap model
										,HttpSession ss 
										,@RequestParam("ma") int ma
										,@RequestParam("ho") String ho
										,@RequestParam("ten") String ten
										,@RequestParam("email") String email
										,@RequestParam("gioiTinh") String gioiTinh
										,@RequestParam("sdt") String sdt
										,@RequestParam("ngaySinh") String ngaySinh
										,@RequestParam("pass") String pass
										,@RequestParam("repass") String repass
										){

		Session sessionCurr = factory.getCurrentSession();
		String hql = "SELECT kh.Sdt FROM KhachHang kh WHERE kh.MaKH <> :id";
		Query query = sessionCurr.createQuery(hql);
		query.setParameter("id", ma);
		List<String> sdtDB = query.list();
		
		for(int i=0; i<sdtDB.size(); i++){
			if(sdt.equals(sdtDB.get(i))){	
				ss.setAttribute("trungSDT", "* Số điện thoại này đã tồn tại");
				sodt = sdt;
				ss.setAttribute("sodt", sodt); //để hiển thị số điện thoại trùng và câu nhắc nhở || nếu k nó sẽ lấy sdt đúng trong database
				return "redirect:/trangCaNhan.htm";
			}
		}
		ss.removeAttribute("trungSDT");
		ss.removeAttribute("sodt");
		
		if(!pass.equals(repass)){
			ss.setAttribute("thatbai", "* Xác nhận thất bại");
			return "redirect:/trangCaNhan.htm";
		}
		ss.removeAttribute("thatbai");
		
		boolean admin = LoginController.ad;
		
		Session sessionCurrPass = factory.getCurrentSession();
		String hqlPass = "FROM KhachHang kh WHERE kh.MaKH = :id";
		Query queryPass = sessionCurrPass.createQuery(hqlPass);
		queryPass.setParameter("id", ma);
		List<KhachHang> passDB = queryPass.list();
		//UPDATE
		
		Session session = factory.openSession();
		Transaction t = session.beginTransaction();
		
		KhachHang kh = new KhachHang();
		kh.setMaKH(ma);
		kh.setHo(ho);
		kh.setTen(ten);
		kh.setEmail(email);
		if(repass.trim().length() == 0){
			kh.setPassword(passDB.get(0).getPassword());
		}else{
			kh.setPassword(repass);
		}
		kh.setSex(gioiTinh);
		kh.setAdmin(admin);
		kh.setSdt(sdt);
		kh.setNgaySinh(ngaySinh);
		
		try {
			session.update(kh);
			t.commit();
		} catch (Exception e) {
			t.rollback();
		}finally {
			session.close();
		}
		
		if((!ho.equals(passDB.get(0).getHo())) || !ten.equals(passDB.get(0).getTen())) {
			return "redirect:/login.htm";
		}
		
		return "redirect:/trangCaNhan.htm";

	}
}
