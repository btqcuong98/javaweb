package ptithcm.controller;

import java.util.List;


import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import ptithcm.entity.KhachHang;


@Transactional
@Controller
public class TkKhachHangController {
	@Autowired
	SessionFactory factory;
	
	//FROM USER
	public List<KhachHang> listKH(){
		Session sessionCurr  = factory.getCurrentSession();
		
		String hql = "FROM KhachHang kh WHERE kh.Admin = 'false'";
		Query query = sessionCurr.createQuery(hql);
		List<KhachHang> listKH = query.list();
		return listKH;
	}
	
	//INDEX
	@RequestMapping("tkKhachHang")
	public String tkKhachHang(ModelMap model){
		model.addAttribute("listKH", listKH());
		return "TkKhachHang";
	}
	
	@RequestMapping("quyen/{id}")
	public String setQuyen(@PathVariable("id") int maKH){
		Session sessionCurr = factory.getCurrentSession();
		String hql = "FROM KhachHang kh WHERE kh.MaKH = :id";
		Query query = sessionCurr.createQuery(hql);
		query.setParameter("id", maKH);
		List<KhachHang> list = query.list();
		
		list.get(0).setAdmin(true);
		
		Session sessionOpen = factory.openSession();
		Transaction t = sessionOpen.beginTransaction();
		
		try {
			sessionOpen.update(list.get(0));
			t.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			sessionOpen.close();
		}
		
		return "redirect:/tkKhachHang.htm";

	}
}
