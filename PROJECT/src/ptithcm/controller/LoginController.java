package ptithcm.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
//import ptithcm.DAO.*;
import ptithcm.entity.*;

@Transactional
@Controller

public class LoginController {
	@Autowired
	SessionFactory factory;
	
	public static  int khDN;
	public static  boolean  ad=false;
	
	//FORM LOGIN
	@RequestMapping(value="login", method=RequestMethod.GET)
	public String login(){
		return "Login";
	}
	
	
	@RequestMapping(value="login", method=RequestMethod.POST)
	public String login(ModelMap model, HttpSession session,
						@RequestParam("username") String username,
						@RequestParam("password") String password){

		
		Session sesion = factory.openSession();
		Transaction t = sesion.beginTransaction();
		String hql = "FROM KhachHang kh WHERE kh.Sdt = :username and  kh.Password = :password";
		Query query = sesion.createQuery(hql);
		query.setParameter("username", username);
		query.setParameter("password", password);
		List<KhachHang> list = query.list();
		if(list.isEmpty()){
			model.addAttribute("user", "* Số điện thoại không đúng");
			model.addAttribute("pass", "* Mật khẩu không đúng");
			return "Login";
		}
		
		khDN = list.get(0).getMaKH();
		ad = list.get(0).getAdmin();
		
		session.setAttribute("mlogin", list.get(0).getHo() + ' '+list.get(0).getTen());
		session.setAttribute("admin", list.get(0).getAdmin());
		session.setAttribute("khDN", khDN);
		return "redirect:/Home.htm";
	}
	
	@RequestMapping("logout")
	public String logout (HttpSession session) {
		session.removeAttribute("mlogin");
		session.removeAttribute("admin");
		session.removeAttribute("khDN");
		//khDN = 0;
		ad = false;
		return "redirect:/Home.htm";
	}
	
}
