package ptithcm.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="NHAHANG")///1: đổi check trong database //2:liên kết khóa ngoại 1-n với món ăn
public class NhaHang {
	@Id @GeneratedValue 
	@Column(name="MANH")
	private int MaNH;
	
	@Column(name="TENNH")
	private String TenNH;
	
	@Column(name="DIENTHOAI")
	private String DienThoai;
	
	@Column(name="KHOANGGIA")
	private String KhoangGia;

	@Column(name="THOIGIANPHUCVU")
	private String ThoiGianPhucVu;
	
	@Column(name="URLHINHANH")
	private String UrlHinhAnh;
	
	@Column(name="DIACHI")
	private String DiaChi;

	@Column(name="TRANGTHAI")
	private String TrangThai;
	
	@OneToMany(mappedBy="nhhang", fetch=FetchType.EAGER)
	private Collection<PhieuDat> phdatNH;
	
	@OneToMany(mappedBy="nhahang", fetch=FetchType.EAGER)
	private Collection<ThongTin> phdat;
	
	public int getMaNH() {
		return MaNH;
	}

	public void setMaNH(int maNH) {
		MaNH = maNH;
	}

	public String getTenNH() {
		return TenNH;
	}

	public void setTenNH(String tenNH) {
		TenNH = tenNH;
	}

	public String getDienThoai() {
		return DienThoai;
	}

	public void setDienThoai(String dienThoai) {
		DienThoai = dienThoai;
	}

	public String getKhoangGia() {
		return KhoangGia;
	}

	public void setKhoangGia(String khoangGia) {
		KhoangGia = khoangGia;
	}

	public String getThoiGianPhucVu() {
		return ThoiGianPhucVu;
	}

	public void setThoiGianPhucVu(String thoiGianPhucVu) {
		ThoiGianPhucVu = thoiGianPhucVu;
	}

	public String getUrlHinhAnh() {
		return UrlHinhAnh;
	}

	public void setUrlHinhAnh(String urlHinhAnh) {
		UrlHinhAnh = urlHinhAnh;
	}

	public String getDiaChi() {
		return DiaChi;
	}

	public void setDiaChi(String diaChi) {
		DiaChi = diaChi;
	}

	public String getTrangThai() {
		return TrangThai;
	}

	public void setTrangThai(String trangThai) {
		TrangThai = trangThai;
	}

	public Collection<PhieuDat> getPhdatNH() {
		return phdatNH;
	}

	public void setPhdatNH(Collection<PhieuDat> phdatNH) {
		this.phdatNH = phdatNH;
	}

	public Collection<ThongTin> getPhdat() {
		return phdat;
	}

	public void setPhdat(Collection<ThongTin> phdat) {
		this.phdat = phdat;
	}
	

	

	
}
