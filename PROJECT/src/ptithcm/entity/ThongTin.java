package ptithcm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="THONGTIN")
public class ThongTin {
	@Id @GeneratedValue
	@Column(name="MATT")
	private int MaTT;
	
	@Column(name="URLHINHANH")
	private String url;
	
	@ManyToOne
	@JoinColumn(name="MANH")
	private NhaHang nhahang;

	public int getMaTT() {
		return MaTT;
	}

	public void setMaTT(int maTT) {
		MaTT = maTT;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public NhaHang getNhahang() {
		return nhahang;
	}

	public void setNhahang(NhaHang nhahang) {
		this.nhahang = nhahang;
	}
	
	
}
