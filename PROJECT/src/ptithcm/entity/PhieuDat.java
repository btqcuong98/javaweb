package ptithcm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PHIEUDAT")
public class PhieuDat {
	
	@Id @GeneratedValue 
	@Column(name="MAPD")
	private int MaPD;
	
	@Column(name="NGAY")
	private String Ngay;
	
	@Column(name="GIO")
	private String Gio;
	
	@Column(name="NGUOILON")
	private String NguoiLon;
	
	@Column(name="TREEM")
	private String TreEm;
	
	@Column(name="GHICHU")
	private String GhiChu;
	
	@Column(name="GIA")
	private Float Gia;
	
	@ManyToOne()
	@JoinColumn(name="MANH")
	private NhaHang nhhang;
	
	@ManyToOne()
	@JoinColumn(name="MAKH")
	private KhachHang khhang;

	public int getMaPD() {
		return MaPD;
	}

	public void setMaPD(int maPD) {
		MaPD = maPD;
	}

	public String getNgay() {
		return Ngay;
	}

	public void setNgay(String ngay) {
		Ngay = ngay;
	}

	public String getGio() {
		return Gio;
	}

	public void setGio(String gio) {
		Gio = gio;
	}

	public String getNguoiLon() {
		return NguoiLon;
	}

	public void setNguoiLon(String nguoiLon) {
		NguoiLon = nguoiLon;
	}
	
	public String getTreEm() {
		return TreEm;
	}

	public void setTreEm(String treEm) {
		TreEm = treEm;
	}

	public String getGhiChu() {
		return GhiChu;
	}

	public void setGhiChu(String ghiChu) {
		GhiChu = ghiChu;
	}

	public Float getGia() {
		return Gia;
	}

	public void setGia(Float gia) {
		Gia = gia;
	}

	public NhaHang getNhhang() {
		return nhhang;
	}

	public void setNhhang(NhaHang nhhang) {
		this.nhhang = nhhang;
	}

	public KhachHang getKhhang() {
		return khhang;
	}

	public void setKhhang(KhachHang khhang) {
		this.khhang = khhang;
	}
	
	
}
