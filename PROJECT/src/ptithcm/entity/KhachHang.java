package ptithcm.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name="KHACHHANG")
public class KhachHang {
	@Id @Column(name="MAKH") @GeneratedValue
	private int MaKH;
	
	@Column(name="HO")
	private String Ho;
	
	@Column(name="TEN")
	private String Ten;
	
	@Column(name="EMAIL")
	private String Email;
	
	@Column(name="PASSWORD")
	private String Password;
	
	@Column(name="SEX")	
	private String Sex;

	@Column(name="ADMIN")	
	private Boolean Admin;
	
	@Column(name="SDT")	
	private String Sdt;

//	@Temporal(TemporalType.DATE)
//	@DateTimeFormat(pattern="yyyy/MM/dd")
	@Column(name="NGAYSINH")	
	private String NgaySinh;
	
	@OneToMany(mappedBy="khhang", fetch=FetchType.EAGER)
	private Collection<PhieuDat> phdat;
	
	public int getMaKH() {
		return MaKH;
	}

	public void setMaKH(int maKH) {
		MaKH = maKH;
	}

	public String getHo() {
		return Ho;
	}

	public void setHo(String ho) {
		Ho = ho;
	}

	public String getTen() {
		return Ten;
	}

	public void setTen(String ten) {
		Ten = ten;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String sex) {
		Sex = sex;
	}

	public Boolean getAdmin() {
		return Admin;
	}

	public void setAdmin(Boolean admin) {
		Admin = admin;
	}

	public String getSdt() {
		return Sdt;
	}

	public void setSdt(String sdt) {
		Sdt = sdt;
	}

	public String getNgaySinh() {
		return NgaySinh;
	}

	public void setNgaySinh(String ngaySinh) {
		NgaySinh = ngaySinh;
	}

	public Collection<PhieuDat> getPhdat() {
		return phdat;
	}

	public void setPhdat(Collection<PhieuDat> phdat) {
		this.phdat = phdat;
	}

	
	
}
